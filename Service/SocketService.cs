using System.Configuration;
using SocketIOClient;
using UndercoverGame.Services;
using UndercoverGame.Model;

namespace UndercoverGame.Service
{
    // https://github.com/doghappy/socket.io-client-csharp

    class SocketService : AService<SocketService>
    {
        public SocketIO client = new SocketIO("http://" + ConfigurationManager.AppSettings["BackendAddress"] + ":" + ConfigurationManager.AppSettings["BackendPort"], new SocketIOOptions
        {
            EIO = 4
        });

        public SocketService()
        {
            
        }

        public void init()
        {
            client.ConnectAsync();

            client.OnConnected += async (sender, e) =>
            {
                await client.EmitAsync(TOPIC.CLIENT.CONNECT_USER, XMLService._instance.GetToken());
            };
        }
    }
}
