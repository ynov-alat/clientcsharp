using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UndercoverGame.Services;

namespace UndercoverGame.Service
{
    class ViewService : AService<ViewService>
    {
        public void OpenView<T>(Form lastForm, string room = "")
        {
            Form newForm;
            if (room.Length > 0)
            {
                newForm = (Form)Activator.CreateInstance(typeof(T), lastForm, room);
            } else
            {

                newForm = (Form)Activator.CreateInstance(typeof(T), lastForm);
            }

            newForm.StartPosition = FormStartPosition.Manual;
            newForm.Left = lastForm.Left;
            newForm.Top = lastForm.Top;
            newForm.ShowDialog();
        }

        public void backToLastView(Form lastform)
        {
            lastform.visible = true;
            this.close();
        }
    }
}
