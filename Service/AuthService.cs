using System.Configuration;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System;
using Newtonsoft.Json;
using UndercoverGame.Services;
using Newtonsoft.Json.Linq;
using UndercoverGame.View;

namespace UndercoverGame.Service
{
    class AuthService : AService<AuthService>
    {

        public User User { get; set; }
        private readonly HttpClient _client;
        private string _backendURL;

        public AuthService() : base()
        {
            _client = new HttpClient();
            _backendURL = "http://" + ConfigurationManager.AppSettings["BackendAddress"] + ":" + ConfigurationManager.AppSettings["BackendPort"] + "/auth/";
        }

        public bool IsTokenValid()
        {
            string token = XMLService._instance.GetToken();

            if (token.Length == 0) return false;            

            if (_client.DefaultRequestHeaders.Authorization != null)
            {
                _client.DefaultRequestHeaders.Remove("authorization");
            }
            
            _client.DefaultRequestHeaders.Add("authorization", token);

            Task<HttpResponseMessage> task = _client.GetAsync(_backendURL + "check-token");
            try
            {
                ResultTask<string>(task);
                SocketService._instance.init();
                User = XMLService._instance.GetUser();
                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        public void Login(string username, string password)
        {
            string jsonParams = JsonConvert.SerializeObject(new { username, password });
            StringContent sc = new StringContent(jsonParams, Encoding.UTF8, "application/json");
            Task<HttpResponseMessage> task = _client.PostAsync(_backendURL + "login", sc);
            try
            {
                SaveUserData(task);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Register(string email, string username, string password)
        {
            string jsonParams = JsonConvert.SerializeObject(new { email = email, username = username, password = password });
            StringContent sc = new StringContent(jsonParams, Encoding.UTF8, "application/json");
            Task<HttpResponseMessage> task = _client.PostAsync(_backendURL + "register", sc);
            try
            {
                SaveUserData(task);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void GetTheme()
        {
            Task<HttpResponseMessage> task = _client.GetAsync("http://" + ConfigurationManager.AppSettings["BackendAddress"] + ":" + ConfigurationManager.AppSettings["BackendPort"] + "/shop/themes/" + this.User.Id);
            try
            {
                 this.User = ResultTask<User>(task);
            }
            catch (Exception ex)
            {
                this.User.BloodyTheme = false;
                this.User.MustardTheme = false;
                this.User.WineTheme = false;
                this.User.SkyTheme = false;
            }
        }
        public void Logout()
        {
            XMLService._instance.SetToken("");
            XMLService._instance.SetUser(new User("", ""));
            XMLService._instance.SetTheme(ViewTemplate.Theme.CLASSIC.ToString());
        }

        public void ChangePassword(string oldPassword, string newPassword)
        {
            string jsonParams = JsonConvert.SerializeObject(new { oldPassword, newPassword });
            Task task = _client.PostAsync(_backendURL + "change-password", new StringContent(jsonParams));
            task.Wait();
            return;
        }

        private void SaveUserData(Task<HttpResponseMessage> task) 
        {
            try
            {
                task.Wait();
                HttpResponseMessage response = task.Result;
                Task<string> jsonString = response.Content.ReadAsStringAsync();
                jsonString.Wait();
                if (response.IsSuccessStatusCode)
                {
                    JObject json = JObject.Parse(jsonString.Result);
                    string username = json["username"].Value<string>();
                    string userId = json["id"].Value<string>();
                    string token = json["token"].Value<string>();
                    XMLService._instance.SetToken(token);
                    User = new User(userId, username);
                    XMLService._instance.SetUser(User);
                    SocketService._instance.init();
                }
                else
                {
                    throw new Exception(((int)response.StatusCode).ToString() + " - " + jsonString.Result);
                }                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private T ResultTask<T>(Task<HttpResponseMessage> task)
        {
            task.Wait();
            HttpResponseMessage response = task.Result;
            Task<string> jsonString = response.Content.ReadAsStringAsync();
            jsonString.Wait();
            if (response.IsSuccessStatusCode)
            {
                return typeof(T) == typeof(string) ? (T)Convert.ChangeType(jsonString.Result, typeof(T)) : JsonConvert.DeserializeObject<T>(jsonString.Result);
            }
            else
            {
                throw new Exception(((int)response.StatusCode).ToString() + " - " + jsonString.Result);
            }
        }
    }
}
