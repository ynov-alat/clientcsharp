using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace UndercoverGame.Services
{
    public abstract class AService<T>
    where T : AService<T>, new()
    {
        public static T _instance = new T();

        public static T Instance
        {
            get
            {
                return _instance;
            }
        }
    }
}