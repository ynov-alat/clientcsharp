using System.Collections.Generic;
using System.Configuration;
using SocketIOClient;
using UndercoverGame.Services;
using UndercoverGame.Model;
using System;
using System.ComponentModel;

namespace UndercoverGame.Service
{

    class LobbySocketService : AService<LobbySocketService>
    {
       
        public LobbySocketService() : base()
        {
        }

        public string IdRoom { get; set; }
        public List<User> Players { get; set; }
        public Parameters Parameters = new Parameters();
        public string WordPlayer { get; set; }
        public ResultModel Result { get; set; }

        #region Listening
        public void ListenPlayersUpdates(Action callback = null)
        {
            SocketService._instance.client.On(TOPIC.SERVER.LOBBY.STATE, response =>
            {
               Players = response.GetValue<List<User>>(0);
               callback?.Invoke();
            });
        }
        public void ListenJoinedLobby(Action callback)
        {
            SocketService._instance.client.On(TOPIC.SERVER.LOBBY.JOINED, response =>
            {
                IdRoom = response.GetValue<string>(0);
                Players = response.GetValue<List<User>>(1);
                callback();
            });
        }
        public void ListenJoinLobbyError(Action<string> callback)
        {
            SocketService._instance.client.On(TOPIC.SERVER.LOBBY.JOIN_ERROR, response =>
            {
                string error = response.GetValue<string>(0);
                callback(error);
            });
        }
        public void ListenStartGame(Action callback)
        {
            SocketService._instance.client.On(TOPIC.SERVER.LOBBY.START_GAME, response =>
            {
                callback();
            });
        }
        public void ListenWordsFromPlayers(Action<string[]> callback)
        {
            SocketService._instance.client.On(TOPIC.SERVER.GAME.PLAYER_TURN, response =>
            {
                string wordAdded = response.GetValue<string>(0) != null ? response.GetValue<string>(0) : "";
                string playerToPlay = response.GetValue<string>(1);
                string turnNumber = response.GetValue<int>(2).ToString();

                callback(new string[] { wordAdded, playerToPlay, turnNumber });
            });
        }
        public void ListenDistributionWords(Action<string> callback)
        {
            SocketService._instance.client.On(TOPIC.SERVER.GAME.PLAYER_THEME, response =>
            {
                WordPlayer = response.GetValue<string>();
                callback(WordPlayer);
            });
        }
        public void ListenPlayerVote(Action<Tuple<string, string>> callback)
        {
            SocketService._instance.client.On(TOPIC.SERVER.GAME.PLAYER_VOTE, response =>
            {
                string playerVote = response.GetValue<string>(0);
                string impostorVoted = response.GetValue<string>(1); 
                
                callback(new Tuple<string, string>(playerVote, impostorVoted)) ;
            });
        }
        public void ListenResults(Action callback)
        {
            SocketService._instance.client.On(TOPIC.SERVER.GAME.RESULTS, response =>
            {
                Result = response.GetValue<ResultModel>();
                callback();
            });
        }
        public void ListenOrderPlayers(Action callback)
        {
            SocketService._instance.client.On(TOPIC.SERVER.GAME.PLAYERS_ORDER, response =>
            {
                List<User> newOrder = new List<User>();                
                List<string> playerOrder = response.GetValue<List<string>>();
                playerOrder.ForEach(x => newOrder.Add(Players.Find(y => y.Id == x)));
                Players = newOrder;                
                callback();
            });
        }

        public void ListenVoteTurn(Action<string> callback)
        {
            SocketService._instance.client.On(TOPIC.SERVER.GAME.VOTE_TURN, response =>
            {
                string lastWord = response.GetValue<string>();
                callback(lastWord);
            });
        }
        #endregion

        #region Emitting
        public void CreateLobby(bool isPrivate)
        {
            SocketService._instance.client.EmitAsync(TOPIC.CLIENT.LOBBY.CREATE, isPrivate);
        }

        public void JoinLobby(string roomId)
        {
            SocketService._instance.client.EmitAsync(TOPIC.CLIENT.LOBBY.JOIN, roomId);
        }
        public void FindLobby()
        {
            SocketService._instance.client.EmitAsync(TOPIC.CLIENT.LOBBY.FIND);
        }

        public void LeaveLobby()
        {
            SocketService._instance.client.EmitAsync(TOPIC.CLIENT.LOBBY.LEAVE, IdRoom);
        }
        public void StartGame()
        {
            SocketService._instance.client.EmitAsync(TOPIC.CLIENT.LOBBY.START_GAME, IdRoom);
        }

        public void SendWord(string word)
        {
            SocketService._instance.client.EmitAsync(TOPIC.CLIENT.GAME.PLAYER_WORD,IdRoom, word);
        }

        public void SendVote(string idsUser)
        {
            SocketService._instance.client.EmitAsync(TOPIC.CLIENT.GAME.PLAYER_VOTE, IdRoom, idsUser);
        }

        public void SendMrWhiteWord(string word)
        {
            SocketService._instance.client.EmitAsync(TOPIC.CLIENT.GAME.MRWHITE_GUESS, IdRoom, word);
        }
        #endregion
    }
}
