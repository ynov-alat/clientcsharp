﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using UndercoverGame.Services;
using UndercoverGame.Model;

namespace UndercoverGame.Service
{
    class XMLService : AService<XMLService>
    {
        private XmlDocument StorageXML { get; }
        private const string PathToXMLStorage = "../../Storage/data.xml";
     
        public XMLService() : base()
        {
            StorageXML = new XmlDocument();
            StorageXML.Load(PathToXMLStorage);    
        }

        #region User
        public string GetToken()
        {
            XmlNode node = StorageXML.DocumentElement.SelectSingleNode("user/token");
            return node.InnerText;
        }

        public void SetToken(string value)
        {
            XmlNode node = StorageXML.DocumentElement.SelectSingleNode("user/token");
            node.InnerText = value;
            StorageXML.Save(PathToXMLStorage);
        }

        public User GetUser()
        {
            XmlNode nodeId = StorageXML.DocumentElement.SelectSingleNode("user/id");
            XmlNode nodeUsername = StorageXML.DocumentElement.SelectSingleNode("user/username");

            return new User(nodeId.InnerText, nodeUsername.InnerText);
        }

        public void SetUser(User user)
        {
            XmlNode nodeId = StorageXML.DocumentElement.SelectSingleNode("user/id");
            XmlNode nodeUsername = StorageXML.DocumentElement.SelectSingleNode("user/username");

            nodeId.InnerText = user.Id;
            nodeUsername.InnerText = user.Username;

            StorageXML.Save(PathToXMLStorage);
        }
        #endregion
        public string GetTheme()
        {
            XmlNode theme = StorageXML.DocumentElement.SelectSingleNode("theme");
            return theme.InnerText;
        }

        public void SetTheme(string newTheme)
        {
            XmlNode theme = StorageXML.DocumentElement.SelectSingleNode("theme");

            theme.InnerText = newTheme;
            StorageXML.Save(PathToXMLStorage);
        }
    }
}