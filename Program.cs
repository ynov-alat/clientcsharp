﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using UndercoverGame.Service;
using UndercoverGame.View;

namespace UndercoverGame
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            InitializeServices();
            InitializeConnection();
        }

        private static void InitializeServices()
        {
            new XMLService();
            new AuthService();            
        }

        private static void InitializeConnection()
        {
            string token = XMLService._instance.GetToken();
            Form form;

            if (token != string.Empty)
            {
                if (AuthService._instance.IsTokenValid())
                {
                    form = new Menu();
                }
                else
                {
                    form = new Register();
                }
            }
            else form = new Register();
            form.StartPosition = FormStartPosition.CenterScreen;
            form.Show();
            Application.Run(form);
        }
    }
}
