﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UndercoverGame.Service;
using UndercoverGame.View;

namespace UndercoverGame
{
    public partial class Play : ViewTemplate
    {
        public Play()
        {
            InitializeComponent();
            InitializePlay();
            InitializeViewWithTheme();
        }

        private void InitializePlay()
        {
            LobbySocketService._instance.ListenJoinedLobby(() => this.Invoke(new MethodInvoker(GoToLobby)));
            LobbySocketService._instance.ListenJoinLobbyError(error => HandleJoinError(error));
        }
        public void InitializeViewWithTheme()
        {
            //Background
            this.BackColor = BackgndColor;

            //Text 
            lbl_createLobby.ForeColor = lbl_findLobby.ForeColor = lbl_joinLobby.ForeColor = cb_isCreatedLobbyPrivate.ForeColor = TextColor;

            //Text error
            lbl_joinError.ForeColor = ErrorColor;

            //Button
            btn_backToMenu.BackColor = btn_createLobby.BackColor = btn_findLobby.BackColor = btn_joinLobby.BackColor = ButtonColor;

            //Text Button
            btn_backToMenu.ForeColor = btn_createLobby.ForeColor = btn_findLobby.ForeColor = btn_joinLobby.ForeColor = TextButtonColor;
        }
        #region Front Events
        private void btn_backToMenu_Click(object sender, EventArgs e)
        {
            Form menuForm = new Menu();
            menuForm.StartPosition = FormStartPosition.Manual;
            menuForm.Left = this.Left;
            menuForm.Top = this.Top;
            this.Visible = false;
            menuForm.Show();
        }

        private void btn_createLobby_Click(object sender, EventArgs e)
        {
            LobbySocketService._instance.CreateLobby(cb_isCreatedLobbyPrivate.Checked);
            ToggleLoading();
        }

        private void btn_joinLobby_Click(object sender, EventArgs e)
        {
            LobbySocketService._instance.JoinLobby(tb_joinLobby.Text);
            ToggleLoading();
        }

        private void btn_findLobby_Click(object sender, EventArgs e)
        {
            LobbySocketService._instance.FindLobby();
            ToggleLoading();
        }
        #endregion

        private void GoToLobby()
        {
            Form lobbyForm = new Lobby();
            lobbyForm.StartPosition = FormStartPosition.Manual;
            lobbyForm.Left = this.Left;
            lobbyForm.Top = this.Top;
            this.Visible = false;
            lobbyForm.Show();
        }

        private string _joinError = "";
        private void HandleJoinError(string error)
        {
            this.Invoke(new MethodInvoker(ToggleLoading));
            _joinError = error;
            this.Invoke(new MethodInvoker(DisplayJoinError));
        }

        private void DisplayJoinError()
        {
            lbl_joinError.Visible = true;
            lbl_joinError.Text = _joinError;
        }

        public void ToggleLoading()
        {
            lbl_joinError.Visible = false;
            pb_loading.Visible = !pb_loading.Visible;
            cb_isCreatedLobbyPrivate.Enabled = !cb_isCreatedLobbyPrivate.Enabled;
            btn_createLobby.Enabled = !btn_createLobby.Enabled;
            tb_joinLobby.Enabled = !tb_joinLobby.Enabled;
            btn_joinLobby.Enabled = !btn_joinLobby.Enabled;
            btn_findLobby.Enabled = !btn_findLobby.Enabled;
        }
    }
}
