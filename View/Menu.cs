﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Sockets;
using UndercoverGame.Service;
using UndercoverGame.View;

namespace UndercoverGame
{
    public partial class Menu : ViewTemplate
    {
        //TODO Adapter la couleur dse icones en fonction du theme
        public Menu()
        {
            InitializeComponent();
            InitializeThemeUser();
            InitializeViewWithTheme();
        }

        public void InitializeThemeUser()
        {
            AuthService._instance.GetTheme();
            InitializeTheme();

        }
        public void InitializeViewWithTheme()
        {
            //Background
            this.BackColor = BackgndColor;

            //Text 
            lbl_gameName.ForeColor = label1.ForeColor = TextColor;

            //Button
            btn_play.BackColor = btn_shop.BackColor = btn_Rules.BackColor = ButtonColor;

            //Text Button
            btn_play.ForeColor = btn_shop.ForeColor = btn_Rules.ForeColor = TextButtonColor;
            
        }
        #region Front Event       
        private void btn_play_Click(object sender, EventArgs e)
        {
            Form playForm = new Play();
            playForm.StartPosition = FormStartPosition.Manual;
            playForm.Left = this.Left;
            playForm.Top = this.Top;
            this.Visible = false;
            playForm.Show();
        }

        private void btn_shop_Click(object sender, EventArgs e)
        {
            Form shopForm = new Shop();
            shopForm.StartPosition = FormStartPosition.Manual;
            shopForm.Left = this.Left;
            shopForm.Top = this.Top;
            this.Visible = false;
            shopForm.Show();
        }
      
        private void btn_logout_Click(object sender, EventArgs e)
        {
            AuthService.Instance.Logout();
            SocketService._instance.client.DisconnectAsync();
            Form registerForm = new Register();
            registerForm.StartPosition = FormStartPosition.Manual;
            registerForm.Left = this.Left;
            registerForm.Top = this.Top;
            this.Visible = false;
            registerForm.Show();
        }
        private void btn_Rules_Click(object sender, EventArgs e)
        {
            Form rulesForm = new Rules();
            rulesForm.StartPosition = FormStartPosition.Manual;
            rulesForm.Left = this.Left;
            rulesForm.Top = this.Top;
            this.Visible = false;
            rulesForm.Show();
        }
        #endregion

    }
}
