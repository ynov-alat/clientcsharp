﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UndercoverGame.View
{
    public partial class Rules : ViewTemplate
    {
        public Rules()
        {
            InitializeComponent();
            InitializeViewWithTheme();
        }
        public void InitializeViewWithTheme()
        {
            //Background
            this.BackColor = BackgndColor;

            //Text 
            label1.ForeColor = label2.ForeColor = label3.ForeColor = label4.ForeColor = label5.ForeColor = label6.ForeColor = label7.ForeColor = label8.ForeColor = label9.ForeColor = TextColor;                     
        }

        private void btn_backToMenu_Click(object sender, EventArgs e)
        {
            Form menuForm = new Menu();
            menuForm.StartPosition = FormStartPosition.Manual;
            menuForm.Left = this.Left;
            menuForm.Top = this.Top;
            this.Visible = false;
            menuForm.Show();
        }
    }
}
