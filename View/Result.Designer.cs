﻿namespace UndercoverGame.View
{
    partial class Result
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_impostorWin = new System.Windows.Forms.Label();
            this.lbl_mrWhiteWin = new System.Windows.Forms.Label();
            this.lbl_civiliansWin = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lbl_guessMrWhite = new System.Windows.Forms.Label();
            this.lbl_civilWord = new System.Windows.Forms.Label();
            this.lbl_impostorWord = new System.Windows.Forms.Label();
            this.tlp_playersRoles = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.pb_civilian = new System.Windows.Forms.PictureBox();
            this.pb_impostor = new System.Windows.Forms.PictureBox();
            this.pb_MrWhite = new System.Windows.Forms.PictureBox();
            this.btn_leaveGame = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_civilian)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_impostor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_MrWhite)).BeginInit();
            this.SuspendLayout();
            // 
            // lbl_impostorWin
            // 
            this.lbl_impostorWin.AutoSize = true;
            this.lbl_impostorWin.Font = new System.Drawing.Font("Calisto MT", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_impostorWin.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lbl_impostorWin.Location = new System.Drawing.Point(454, 23);
            this.lbl_impostorWin.Name = "lbl_impostorWin";
            this.lbl_impostorWin.Size = new System.Drawing.Size(273, 28);
            this.lbl_impostorWin.TabIndex = 19;
            this.lbl_impostorWin.Text = "THE IMPOSTOR WINS";
            this.lbl_impostorWin.Visible = false;
            // 
            // lbl_mrWhiteWin
            // 
            this.lbl_mrWhiteWin.AutoSize = true;
            this.lbl_mrWhiteWin.Font = new System.Drawing.Font("Calisto MT", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_mrWhiteWin.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lbl_mrWhiteWin.Location = new System.Drawing.Point(454, 62);
            this.lbl_mrWhiteWin.Name = "lbl_mrWhiteWin";
            this.lbl_mrWhiteWin.Size = new System.Drawing.Size(308, 28);
            this.lbl_mrWhiteWin.TabIndex = 20;
            this.lbl_mrWhiteWin.Text = "... AND MR WHITE TOO !";
            this.lbl_mrWhiteWin.Visible = false;
            // 
            // lbl_civiliansWin
            // 
            this.lbl_civiliansWin.AutoSize = true;
            this.lbl_civiliansWin.Font = new System.Drawing.Font("Calisto MT", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_civiliansWin.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lbl_civiliansWin.Location = new System.Drawing.Point(471, 23);
            this.lbl_civiliansWin.Name = "lbl_civiliansWin";
            this.lbl_civiliansWin.Size = new System.Drawing.Size(258, 28);
            this.lbl_civiliansWin.TabIndex = 21;
            this.lbl_civiliansWin.Text = "THE CIVILIANS WIN";
            this.lbl_civiliansWin.Visible = false;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calisto MT", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label2.Location = new System.Drawing.Point(3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(196, 75);
            this.label2.TabIndex = 22;
            this.label2.Text = "Civil word :";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Calisto MT", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label1.Location = new System.Drawing.Point(3, 75);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(196, 75);
            this.label1.TabIndex = 23;
            this.label1.Text = "Impostor word :";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calisto MT", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label3.Location = new System.Drawing.Point(3, 150);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(196, 77);
            this.label3.TabIndex = 24;
            this.label3.Text = "Mr White guess :";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbl_guessMrWhite
            // 
            this.lbl_guessMrWhite.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_guessMrWhite.AutoSize = true;
            this.lbl_guessMrWhite.Font = new System.Drawing.Font("Calisto MT", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_guessMrWhite.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lbl_guessMrWhite.Location = new System.Drawing.Point(205, 150);
            this.lbl_guessMrWhite.Name = "lbl_guessMrWhite";
            this.lbl_guessMrWhite.Size = new System.Drawing.Size(197, 77);
            this.lbl_guessMrWhite.TabIndex = 25;
            this.lbl_guessMrWhite.Text = "-------";
            this.lbl_guessMrWhite.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbl_civilWord
            // 
            this.lbl_civilWord.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_civilWord.AutoSize = true;
            this.lbl_civilWord.Font = new System.Drawing.Font("Calisto MT", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_civilWord.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lbl_civilWord.Location = new System.Drawing.Point(205, 0);
            this.lbl_civilWord.Name = "lbl_civilWord";
            this.lbl_civilWord.Size = new System.Drawing.Size(197, 75);
            this.lbl_civilWord.TabIndex = 26;
            this.lbl_civilWord.Text = "-------";
            this.lbl_civilWord.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbl_impostorWord
            // 
            this.lbl_impostorWord.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_impostorWord.AutoSize = true;
            this.lbl_impostorWord.Font = new System.Drawing.Font("Calisto MT", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_impostorWord.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lbl_impostorWord.Location = new System.Drawing.Point(205, 75);
            this.lbl_impostorWord.Name = "lbl_impostorWord";
            this.lbl_impostorWord.Size = new System.Drawing.Size(197, 75);
            this.lbl_impostorWord.TabIndex = 27;
            this.lbl_impostorWord.Text = "-------";
            this.lbl_impostorWord.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tlp_playersRoles
            // 
            this.tlp_playersRoles.ColumnCount = 1;
            this.tlp_playersRoles.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlp_playersRoles.Location = new System.Drawing.Point(252, 106);
            this.tlp_playersRoles.Name = "tlp_playersRoles";
            this.tlp_playersRoles.RowCount = 1;
            this.tlp_playersRoles.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlp_playersRoles.Size = new System.Drawing.Size(357, 227);
            this.tlp_playersRoles.TabIndex = 28;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.lbl_guessMrWhite, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.lbl_civilWord, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.lbl_impostorWord, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(624, 106);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(405, 227);
            this.tableLayoutPanel1.TabIndex = 29;
            // 
            // pb_civilian
            // 
            this.pb_civilian.Image = global::UndercoverGame.Properties.Resources.civilian;
            this.pb_civilian.Location = new System.Drawing.Point(322, 348);
            this.pb_civilian.Name = "pb_civilian";
            this.pb_civilian.Size = new System.Drawing.Size(519, 442);
            this.pb_civilian.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_civilian.TabIndex = 18;
            this.pb_civilian.TabStop = false;
            this.pb_civilian.Visible = false;
            // 
            // pb_impostor
            // 
            this.pb_impostor.Image = global::UndercoverGame.Properties.Resources.impostor;
            this.pb_impostor.Location = new System.Drawing.Point(811, 23);
            this.pb_impostor.Name = "pb_impostor";
            this.pb_impostor.Size = new System.Drawing.Size(771, 684);
            this.pb_impostor.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_impostor.TabIndex = 17;
            this.pb_impostor.TabStop = false;
            this.pb_impostor.Visible = false;
            // 
            // pb_MrWhite
            // 
            this.pb_MrWhite.Image = global::UndercoverGame.Properties.Resources.detective;
            this.pb_MrWhite.Location = new System.Drawing.Point(-411, 23);
            this.pb_MrWhite.Name = "pb_MrWhite";
            this.pb_MrWhite.Size = new System.Drawing.Size(771, 684);
            this.pb_MrWhite.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_MrWhite.TabIndex = 16;
            this.pb_MrWhite.TabStop = false;
            this.pb_MrWhite.Visible = false;
            // 
            // btn_leaveGame
            // 
            this.btn_leaveGame.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_leaveGame.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_leaveGame.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_leaveGame.ForeColor = System.Drawing.Color.White;
            this.btn_leaveGame.Location = new System.Drawing.Point(996, 631);
            this.btn_leaveGame.Name = "btn_leaveGame";
            this.btn_leaveGame.Size = new System.Drawing.Size(178, 47);
            this.btn_leaveGame.TabIndex = 31;
            this.btn_leaveGame.Text = "Return to lobby";
            this.btn_leaveGame.UseVisualStyleBackColor = false;
            this.btn_leaveGame.Click += new System.EventHandler(this.btn_leaveGame_Click);
            // 
            // Result
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1186, 690);
            this.Controls.Add(this.btn_leaveGame);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.tlp_playersRoles);
            this.Controls.Add(this.lbl_civiliansWin);
            this.Controls.Add(this.lbl_mrWhiteWin);
            this.Controls.Add(this.lbl_impostorWin);
            this.Controls.Add(this.pb_civilian);
            this.Controls.Add(this.pb_impostor);
            this.Controls.Add(this.pb_MrWhite);
            this.MinimumSize = new System.Drawing.Size(1202, 729);
            this.Name = "Result";
            this.Text = "Result";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_civilian)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_impostor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_MrWhite)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pb_MrWhite;
        private System.Windows.Forms.PictureBox pb_impostor;
        private System.Windows.Forms.PictureBox pb_civilian;
        private System.Windows.Forms.Label lbl_impostorWin;
        private System.Windows.Forms.Label lbl_mrWhiteWin;
        private System.Windows.Forms.Label lbl_civiliansWin;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lbl_guessMrWhite;
        private System.Windows.Forms.Label lbl_civilWord;
        private System.Windows.Forms.Label lbl_impostorWord;
        private System.Windows.Forms.TableLayoutPanel tlp_playersRoles;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button btn_leaveGame;
    }
}