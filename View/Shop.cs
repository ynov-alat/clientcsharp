﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UndercoverGame.View;
using UndercoverGame.Service;
using System.Diagnostics;

namespace UndercoverGame
{
    public partial class Shop : ViewTemplate
    {
        public Shop()
        {
            InitializeComponent();
            InitializeViewWithTheme();
        }
        public void InitializeViewWithTheme()
        {
            //Background
            this.BackColor = BackgndColor;

            //Text error
            lbl_themeLock.ForeColor = ErrorColor;

            //Button
            btn_backToMenu.BackColor = btn_visitShop.BackColor = ButtonColor;

            //Text Button
            btn_backToMenu.ForeColor = btn_visitShop.ForeColor = TextButtonColor;            
        }
        #region Front Event
        private void btn_backToMenu_Click(object sender, EventArgs e)
        {
            Form menuForm = new Menu();
            menuForm.StartPosition = FormStartPosition.Manual;
            menuForm.Left = this.Left;
            menuForm.Top = this.Top;
            this.Visible = false;
            menuForm.Show();
        }

        private void ClassicTheme_Click(object sender, EventArgs e)
        {
            lbl_themeLock.Visible = false;
            UnselectTheme();
            ClassicTheme.BorderStyle = BorderStyle.Fixed3D;
            XMLService._instance.SetTheme(Theme.CLASSIC.ToString());
            InitializeTheme();
            InitializeViewWithTheme();
        }

        private void BloodyTheme_Click(object sender, EventArgs e)
        {
            lbl_themeLock.Visible = false;

            AuthService._instance.GetTheme();
            if (AuthService._instance.User.BloodyTheme == true)
            {
                UnselectTheme();
                BloodyTheme.BorderStyle = BorderStyle.Fixed3D;
                XMLService._instance.SetTheme(Theme.BLOODY.ToString());
                InitializeTheme();
                InitializeViewWithTheme();
            }
            else lbl_themeLock.Visible = true;
        }

        private void MustardTheme_Click(object sender, EventArgs e)
        {
            lbl_themeLock.Visible = false;

            AuthService._instance.GetTheme();
            if (AuthService._instance.User.MustardTheme == true)
            {
                UnselectTheme();
                MustardTheme.BorderStyle = BorderStyle.Fixed3D;
                XMLService._instance.SetTheme(Theme.MUSTARD.ToString());
                InitializeTheme();
                InitializeViewWithTheme();
            }
            else lbl_themeLock.Visible = true;
        }

        private void WineTheme_Click(object sender, EventArgs e)
        {
            lbl_themeLock.Visible = false ;
            AuthService._instance.GetTheme();
            if (AuthService._instance.User.WineTheme == true)
            {
                UnselectTheme();
                WineTheme.BorderStyle = BorderStyle.Fixed3D;
                XMLService._instance.SetTheme(Theme.WINE.ToString());
                InitializeTheme();
                InitializeViewWithTheme();
            }
            else lbl_themeLock.Visible = true;
        }

        private void SkyTheme_Click(object sender, EventArgs e)
        {
            lbl_themeLock.Visible = false;
            AuthService._instance.GetTheme();
            if (AuthService._instance.User.SkyTheme == true)
            {
                UnselectTheme();
                SkyTheme.BorderStyle = BorderStyle.Fixed3D;
                XMLService._instance.SetTheme(Theme.SKY.ToString());
                InitializeTheme();
                InitializeViewWithTheme();
            }
            else lbl_themeLock.Visible = true;
        }
        private void btn_visitShop_Click(object sender, EventArgs e)
        {
            Process.Start("http://localhost:4200/shop/"+XMLService._instance.GetToken());
        }       
        #endregion

        public void UnselectTheme()
        {
            ClassicTheme.BorderStyle = BorderStyle.None;
            BloodyTheme.BorderStyle = BorderStyle.None;
            MustardTheme.BorderStyle = BorderStyle.None;
            WineTheme.BorderStyle = BorderStyle.None;
            SkyTheme.BorderStyle = BorderStyle.None;
        }

    }
}
