﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using System.Windows.Forms;
using UndercoverGame.Service;

namespace UndercoverGame.View
{
    public partial class Register : ViewTemplate
    {
        public Register()
        {
            InitializeComponent();
        }

        private bool IsFieldCorrect(bool isLogin)
        {
            bool isFieldCorrect = true; ;
            Regex rgUsername = new Regex("^[a-zA-Z0-9_-]{3,16}$");
            Regex rgPassword = new Regex("(?=.*?[A-Z])[a-zA-Z0-9]{8,}");
            SwitchOffErrorLabels();

            EmailAddressAttribute verifMail = new EmailAddressAttribute();
            if((tb_email.Text == string.Empty && !isLogin) || tb_username.Text == string.Empty || tb_password.Text == string.Empty)
            {
                lbl_fieldsRequired.Visible = true;
                isFieldCorrect = false;
                return false;
            }

            if (!verifMail.IsValid(tb_email.Text) && !isLogin)
            {
                lbl_emailError.Visible = true;
                isFieldCorrect = false;
            }
            if (!rgUsername.IsMatch(tb_username.Text) && !isLogin)
            {
                lbl_usernameError.Visible = true;
                isFieldCorrect = false;
            }
            if (!rgPassword.IsMatch(tb_password.Text) && !isLogin)
            {
                lbl_passwordError.Visible = true;
                isFieldCorrect = false;
            }
          
            return isFieldCorrect;
        }
        private void SwitchLoginRegistration()
        {
            SwitchOffErrorLabels();
            if (lbl_login.BorderStyle == BorderStyle.Fixed3D)
            {
                lbl_login.BorderStyle = BorderStyle.None;
                lbl_registration.BorderStyle = BorderStyle.Fixed3D;
            }
            else { 
                lbl_login.BorderStyle = BorderStyle.Fixed3D;
                lbl_registration.BorderStyle = BorderStyle.None;               
            }
            lbl_email.Visible = !lbl_email.Visible;
            tb_email.Visible = !tb_email.Visible;
            btn_login.Visible = !btn_login.Visible;
            btn_register.Visible = !btn_register.Visible;

        }

        private void SwitchOffErrorLabels()
        {
            lbl_fieldsRequired.Visible = false;
            lbl_emailError.Visible = false;
            lbl_usernameError.Visible = false;
            lbl_passwordError.Visible = false;
            lbl_loginError.Visible = false;
        }
        #region Front Event
        private void btn_register_Click(object sender, EventArgs e)
        {
            if (IsFieldCorrect(false))
            {
                try
                {
                    AuthService._instance.Register(tb_email.Text, tb_username.Text, tb_password.Text);
                    Form menuForm = new Menu();
                    menuForm.StartPosition = FormStartPosition.Manual;
                    menuForm.Left = this.Left;
                    menuForm.Top = this.Top;
                    this.Visible = false;
                    menuForm.Show();
                }
                catch (Exception ex)
                {
                    lbl_loginError.Text = ex.Message;
                    lbl_loginError.Visible = true;

                }
            }
        }

        private void lbl_login_Click(object sender, EventArgs e)
        {
            SwitchLoginRegistration();
        }
     

        private void lbl_registration_Click(object sender, EventArgs e)
        {
            SwitchLoginRegistration();
        }

        private void btn_login_Click(object sender, EventArgs e)
        {
            if (IsFieldCorrect(true))
            {
                try
                {
                    AuthService._instance.Login(tb_username.Text, tb_password.Text);

                    ////ouvrir le menu et fermer cette fenêtre
                    Form menuForm = new Menu();
                    menuForm.StartPosition = FormStartPosition.Manual;
                    menuForm.Left = this.Left;
                    menuForm.Top = this.Top;
                    this.Visible = false;
                    menuForm.Show();
                }
                catch (Exception ex)
                {
                    lbl_loginError.Text = ex.Message;
                    lbl_loginError.Visible = true;
                }
            }
        }
        #endregion
    }
}
