﻿namespace UndercoverGame.View
{
    partial class Rules
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.pb_civilian = new System.Windows.Forms.PictureBox();
            this.pb_impostor = new System.Windows.Forms.PictureBox();
            this.pb_MrWhite = new System.Windows.Forms.PictureBox();
            this.btn_backToMenu = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pb_civilian)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_impostor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_MrWhite)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calisto MT", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(159, 34);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(337, 21);
            this.label1.TabIndex = 0;
            this.label1.Text = "The roles are Civil, Impostor, Mr White";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calisto MT", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(159, 71);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(836, 21);
            this.label2.TabIndex = 1;
            this.label2.Text = "Civilians have one word in common. Their goal is to find out who the impostor is " +
    "and eliminate him.";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calisto MT", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(159, 116);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(585, 21);
            this.label3.TabIndex = 2;
            this.label3.Text = "The impostor has a different word but the same theme as the civilians. ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calisto MT", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(159, 217);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(605, 21);
            this.label4.TabIndex = 3;
            this.label4.Text = "Each player will take turns writing a word related to the one he received.";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calisto MT", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(159, 138);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(694, 21);
            this.label5.TabIndex = 4;
            this.label5.Text = "His goal is to remain incognito and to make someone else take the blame in the en" +
    "d.";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calisto MT", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(159, 180);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(660, 21);
            this.label6.TabIndex = 5;
            this.label6.Text = "Mr. White does not have any word. His goal is to find the word of the civilians.";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calisto MT", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(159, 258);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(352, 21);
            this.label7.TabIndex = 6;
            this.label7.Text = "At the final turn, one player is eliminated.";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calisto MT", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(159, 279);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(589, 21);
            this.label8.TabIndex = 7;
            this.label8.Text = "If it is the impostor, the civilians win. If it is not the impostor, he wins.";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Calisto MT", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(159, 301);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(470, 21);
            this.label9.TabIndex = 8;
            this.label9.Text = "Mr. White also wins if he finds the word of the civilians.";
            // 
            // pb_civilian
            // 
            this.pb_civilian.Image = global::UndercoverGame.Properties.Resources.civilian;
            this.pb_civilian.Location = new System.Drawing.Point(400, 358);
            this.pb_civilian.Name = "pb_civilian";
            this.pb_civilian.Size = new System.Drawing.Size(518, 407);
            this.pb_civilian.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_civilian.TabIndex = 21;
            this.pb_civilian.TabStop = false;
            // 
            // pb_impostor
            // 
            this.pb_impostor.Image = global::UndercoverGame.Properties.Resources.impostor;
            this.pb_impostor.Location = new System.Drawing.Point(864, 366);
            this.pb_impostor.Name = "pb_impostor";
            this.pb_impostor.Size = new System.Drawing.Size(392, 334);
            this.pb_impostor.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_impostor.TabIndex = 20;
            this.pb_impostor.TabStop = false;
            // 
            // pb_MrWhite
            // 
            this.pb_MrWhite.Image = global::UndercoverGame.Properties.Resources.detective;
            this.pb_MrWhite.Location = new System.Drawing.Point(57, 358);
            this.pb_MrWhite.Name = "pb_MrWhite";
            this.pb_MrWhite.Size = new System.Drawing.Size(388, 374);
            this.pb_MrWhite.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_MrWhite.TabIndex = 19;
            this.pb_MrWhite.TabStop = false;
            // 
            // btn_backToMenu
            // 
            this.btn_backToMenu.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_backToMenu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_backToMenu.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_backToMenu.ForeColor = System.Drawing.Color.White;
            this.btn_backToMenu.Location = new System.Drawing.Point(1078, 631);
            this.btn_backToMenu.Name = "btn_backToMenu";
            this.btn_backToMenu.Size = new System.Drawing.Size(178, 47);
            this.btn_backToMenu.TabIndex = 32;
            this.btn_backToMenu.Text = "Return to Menu";
            this.btn_backToMenu.UseVisualStyleBackColor = false;
            this.btn_backToMenu.Click += new System.EventHandler(this.btn_backToMenu_Click);
            // 
            // Rules
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1268, 690);
            this.Controls.Add(this.btn_backToMenu);
            this.Controls.Add(this.pb_impostor);
            this.Controls.Add(this.pb_MrWhite);
            this.Controls.Add(this.pb_civilian);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MinimumSize = new System.Drawing.Size(1284, 729);
            this.Name = "Rules";
            this.Text = "Rules";
            ((System.ComponentModel.ISupportInitialize)(this.pb_civilian)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_impostor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_MrWhite)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.PictureBox pb_civilian;
        private System.Windows.Forms.PictureBox pb_impostor;
        private System.Windows.Forms.PictureBox pb_MrWhite;
        private System.Windows.Forms.Button btn_backToMenu;
    }
}