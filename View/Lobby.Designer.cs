﻿using System;
using System.Windows.Forms;

namespace UndercoverGame
{
    partial class Lobby
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btn_leaveLobby = new System.Windows.Forms.Button();
            this.btn_startGame = new System.Windows.Forms.Button();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tlp_parameters = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_parameters = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_impostorNumberValue = new System.Windows.Forms.Label();
            this.lbl_imposterNumber = new System.Windows.Forms.Label();
            this.lbl_timeToWrite = new System.Windows.Forms.Label();
            this.lbl_timeToVote = new System.Windows.Forms.Label();
            this.lbl_turnMaxValue = new System.Windows.Forms.Label();
            this.lbl_timeToWriteValue = new System.Windows.Forms.Label();
            this.lbl_timeToVoteValue = new System.Windows.Forms.Label();
            this.lbl_turnMax = new System.Windows.Forms.Label();
            this.tlp_playersArea = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_players = new System.Windows.Forms.Label();
            this.tlp_players = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_playerNumber = new System.Windows.Forms.Label();
            this.lbl_room = new System.Windows.Forms.Label();
            this.lbl_playerNumberWarning = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.tlp_parameters.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tlp_playersArea.SuspendLayout();
            this.tlp_players.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_leaveLobby
            // 
            this.btn_leaveLobby.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_leaveLobby.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_leaveLobby.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_leaveLobby.ForeColor = System.Drawing.Color.White;
            this.btn_leaveLobby.Location = new System.Drawing.Point(1079, 632);
            this.btn_leaveLobby.Name = "btn_leaveLobby";
            this.btn_leaveLobby.Size = new System.Drawing.Size(178, 47);
            this.btn_leaveLobby.TabIndex = 3;
            this.btn_leaveLobby.Text = "Leave the lobby";
            this.btn_leaveLobby.UseVisualStyleBackColor = false;
            this.btn_leaveLobby.Click += new System.EventHandler(this.btn_leaveLobby_Click);
            // 
            // btn_startGame
            // 
            this.btn_startGame.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btn_startGame.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(53)))), ((int)(((byte)(58)))));
            this.btn_startGame.Enabled = false;
            this.btn_startGame.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_startGame.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_startGame.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_startGame.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_startGame.Location = new System.Drawing.Point(219, 576);
            this.btn_startGame.Margin = new System.Windows.Forms.Padding(4);
            this.btn_startGame.Name = "btn_startGame";
            this.btn_startGame.Size = new System.Drawing.Size(198, 67);
            this.btn_startGame.TabIndex = 4;
            this.btn_startGame.Text = "Start the game";
            this.btn_startGame.UseVisualStyleBackColor = false;
            this.btn_startGame.Visible = false;
            this.btn_startGame.Click += new System.EventHandler(this.btn_startGame_Click);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // contextMenuStrip2
            // 
            this.contextMenuStrip2.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip2.Name = "contextMenuStrip2";
            this.contextMenuStrip2.Size = new System.Drawing.Size(61, 4);
            // 
            // tlp_parameters
            // 
            this.tlp_parameters.ColumnCount = 1;
            this.tlp_parameters.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlp_parameters.Controls.Add(this.lbl_parameters, 0, 0);
            this.tlp_parameters.Controls.Add(this.tableLayoutPanel1, 0, 1);
            this.tlp_parameters.Location = new System.Drawing.Point(654, 3);
            this.tlp_parameters.Name = "tlp_parameters";
            this.tlp_parameters.RowCount = 3;
            this.tlp_parameters.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.50785F));
            this.tlp_parameters.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 66.49215F));
            this.tlp_parameters.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlp_parameters.Size = new System.Drawing.Size(421, 273);
            this.tlp_parameters.TabIndex = 9;
            // 
            // lbl_parameters
            // 
            this.lbl_parameters.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_parameters.AutoSize = true;
            this.lbl_parameters.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_parameters.Font = new System.Drawing.Font("Calisto MT", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_parameters.ForeColor = System.Drawing.Color.White;
            this.lbl_parameters.Location = new System.Drawing.Point(3, 0);
            this.lbl_parameters.Name = "lbl_parameters";
            this.lbl_parameters.Size = new System.Drawing.Size(415, 84);
            this.lbl_parameters.TabIndex = 9;
            this.lbl_parameters.Text = "Parameters";
            this.lbl_parameters.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.lbl_impostorNumberValue, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.lbl_imposterNumber, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.lbl_timeToWrite, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.lbl_timeToVote, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.lbl_turnMaxValue, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.lbl_timeToWriteValue, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.lbl_timeToVoteValue, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.lbl_turnMax, 0, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 87);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(415, 162);
            this.tableLayoutPanel1.TabIndex = 10;
            // 
            // lbl_impostorNumberValue
            // 
            this.lbl_impostorNumberValue.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_impostorNumberValue.AutoSize = true;
            this.lbl_impostorNumberValue.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_impostorNumberValue.Font = new System.Drawing.Font("Calisto MT", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_impostorNumberValue.ForeColor = System.Drawing.Color.White;
            this.lbl_impostorNumberValue.Location = new System.Drawing.Point(210, 0);
            this.lbl_impostorNumberValue.Name = "lbl_impostorNumberValue";
            this.lbl_impostorNumberValue.Size = new System.Drawing.Size(202, 40);
            this.lbl_impostorNumberValue.TabIndex = 12;
            this.lbl_impostorNumberValue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_imposterNumber
            // 
            this.lbl_imposterNumber.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_imposterNumber.AutoSize = true;
            this.lbl_imposterNumber.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_imposterNumber.Font = new System.Drawing.Font("Calisto MT", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_imposterNumber.ForeColor = System.Drawing.Color.White;
            this.lbl_imposterNumber.Location = new System.Drawing.Point(3, 0);
            this.lbl_imposterNumber.Name = "lbl_imposterNumber";
            this.lbl_imposterNumber.Size = new System.Drawing.Size(201, 40);
            this.lbl_imposterNumber.TabIndex = 11;
            this.lbl_imposterNumber.Text = "Impostor number";
            this.lbl_imposterNumber.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_timeToWrite
            // 
            this.lbl_timeToWrite.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_timeToWrite.AutoSize = true;
            this.lbl_timeToWrite.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_timeToWrite.Font = new System.Drawing.Font("Calisto MT", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_timeToWrite.ForeColor = System.Drawing.Color.White;
            this.lbl_timeToWrite.Location = new System.Drawing.Point(3, 80);
            this.lbl_timeToWrite.Name = "lbl_timeToWrite";
            this.lbl_timeToWrite.Size = new System.Drawing.Size(201, 40);
            this.lbl_timeToWrite.TabIndex = 14;
            this.lbl_timeToWrite.Text = "Time to write";
            this.lbl_timeToWrite.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_timeToVote
            // 
            this.lbl_timeToVote.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_timeToVote.AutoSize = true;
            this.lbl_timeToVote.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_timeToVote.Font = new System.Drawing.Font("Calisto MT", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_timeToVote.ForeColor = System.Drawing.Color.White;
            this.lbl_timeToVote.Location = new System.Drawing.Point(3, 120);
            this.lbl_timeToVote.Name = "lbl_timeToVote";
            this.lbl_timeToVote.Size = new System.Drawing.Size(201, 42);
            this.lbl_timeToVote.TabIndex = 15;
            this.lbl_timeToVote.Text = "Time to vote";
            this.lbl_timeToVote.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_turnMaxValue
            // 
            this.lbl_turnMaxValue.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_turnMaxValue.AutoSize = true;
            this.lbl_turnMaxValue.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_turnMaxValue.Font = new System.Drawing.Font("Calisto MT", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_turnMaxValue.ForeColor = System.Drawing.Color.White;
            this.lbl_turnMaxValue.Location = new System.Drawing.Point(210, 40);
            this.lbl_turnMaxValue.Name = "lbl_turnMaxValue";
            this.lbl_turnMaxValue.Size = new System.Drawing.Size(202, 40);
            this.lbl_turnMaxValue.TabIndex = 16;
            this.lbl_turnMaxValue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_timeToWriteValue
            // 
            this.lbl_timeToWriteValue.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_timeToWriteValue.AutoSize = true;
            this.lbl_timeToWriteValue.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_timeToWriteValue.Font = new System.Drawing.Font("Calisto MT", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_timeToWriteValue.ForeColor = System.Drawing.Color.White;
            this.lbl_timeToWriteValue.Location = new System.Drawing.Point(210, 80);
            this.lbl_timeToWriteValue.Name = "lbl_timeToWriteValue";
            this.lbl_timeToWriteValue.Size = new System.Drawing.Size(202, 40);
            this.lbl_timeToWriteValue.TabIndex = 17;
            this.lbl_timeToWriteValue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_timeToVoteValue
            // 
            this.lbl_timeToVoteValue.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_timeToVoteValue.AutoSize = true;
            this.lbl_timeToVoteValue.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_timeToVoteValue.Font = new System.Drawing.Font("Calisto MT", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_timeToVoteValue.ForeColor = System.Drawing.Color.White;
            this.lbl_timeToVoteValue.Location = new System.Drawing.Point(210, 120);
            this.lbl_timeToVoteValue.Name = "lbl_timeToVoteValue";
            this.lbl_timeToVoteValue.Size = new System.Drawing.Size(202, 42);
            this.lbl_timeToVoteValue.TabIndex = 18;
            this.lbl_timeToVoteValue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_turnMax
            // 
            this.lbl_turnMax.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_turnMax.AutoSize = true;
            this.lbl_turnMax.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_turnMax.Font = new System.Drawing.Font("Calisto MT", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_turnMax.ForeColor = System.Drawing.Color.White;
            this.lbl_turnMax.Location = new System.Drawing.Point(3, 40);
            this.lbl_turnMax.Name = "lbl_turnMax";
            this.lbl_turnMax.Size = new System.Drawing.Size(201, 40);
            this.lbl_turnMax.TabIndex = 13;
            this.lbl_turnMax.Text = "Turn number";
            this.lbl_turnMax.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tlp_playersArea
            // 
            this.tlp_playersArea.ColumnCount = 1;
            this.tlp_playersArea.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlp_playersArea.Controls.Add(this.tlp_players, 0, 1);
            this.tlp_playersArea.Controls.Add(this.lbl_players, 0, 0);
            this.tlp_playersArea.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tlp_playersArea.Location = new System.Drawing.Point(139, 100);
            this.tlp_playersArea.Name = "tlp_playersArea";
            this.tlp_playersArea.RowCount = 2;
            this.tlp_playersArea.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15.78947F));
            this.tlp_playersArea.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 84.21053F));
            this.tlp_playersArea.Size = new System.Drawing.Size(363, 457);
            this.tlp_playersArea.TabIndex = 10;
            // 
            // lbl_players
            // 
            this.lbl_players.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_players.AutoSize = true;
            this.lbl_players.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_players.Font = new System.Drawing.Font("Calisto MT", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_players.ForeColor = System.Drawing.Color.White;
            this.lbl_players.Location = new System.Drawing.Point(2, 0);
            this.lbl_players.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_players.Name = "lbl_players";
            this.lbl_players.Size = new System.Drawing.Size(359, 72);
            this.lbl_players.TabIndex = 10;
            this.lbl_players.Text = "Players";
            this.lbl_players.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tlp_players
            // 
            this.tlp_players.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tlp_players.ColumnCount = 1;
            this.tlp_players.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlp_players.Controls.Add(this.lbl_playerNumber, 0, 0);
            this.tlp_players.Location = new System.Drawing.Point(3, 75);
            this.tlp_players.Name = "tlp_players";
            this.tlp_players.RowCount = 11;
            this.tlp_players.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
            this.tlp_players.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
            this.tlp_players.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
            this.tlp_players.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
            this.tlp_players.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
            this.tlp_players.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
            this.tlp_players.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
            this.tlp_players.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
            this.tlp_players.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
            this.tlp_players.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
            this.tlp_players.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
            this.tlp_players.Size = new System.Drawing.Size(357, 379);
            this.tlp_players.TabIndex = 11;
            // 
            // lbl_playerNumber
            // 
            this.lbl_playerNumber.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_playerNumber.AutoSize = true;
            this.lbl_playerNumber.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_playerNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_playerNumber.ForeColor = System.Drawing.Color.White;
            this.lbl_playerNumber.Location = new System.Drawing.Point(3, 0);
            this.lbl_playerNumber.Name = "lbl_playerNumber";
            this.lbl_playerNumber.Size = new System.Drawing.Size(351, 22);
            this.lbl_playerNumber.TabIndex = 14;
            this.lbl_playerNumber.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_room
            // 
            this.lbl_room.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_room.AutoSize = true;
            this.lbl_room.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_room.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_room.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.lbl_room.Location = new System.Drawing.Point(466, 13);
            this.lbl_room.Name = "lbl_room";
            this.lbl_room.Size = new System.Drawing.Size(0, 25);
            this.lbl_room.TabIndex = 11;
            this.lbl_room.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_playerNumberWarning
            // 
            this.lbl_playerNumberWarning.AutoSize = true;
            this.lbl_playerNumberWarning.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_playerNumberWarning.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.lbl_playerNumberWarning.Location = new System.Drawing.Point(169, 653);
            this.lbl_playerNumberWarning.Name = "lbl_playerNumberWarning";
            this.lbl_playerNumberWarning.Size = new System.Drawing.Size(333, 25);
            this.lbl_playerNumberWarning.TabIndex = 12;
            this.lbl_playerNumberWarning.Text = "4 players are required to start a game";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::UndercoverGame.Properties.Resources.civilian;
            this.pictureBox2.Location = new System.Drawing.Point(523, 197);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(684, 656);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 17;
            this.pictureBox2.TabStop = false;
            // 
            // Lobby
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(53)))), ((int)(((byte)(59)))));
            this.ClientSize = new System.Drawing.Size(1269, 691);
            this.Controls.Add(this.lbl_playerNumberWarning);
            this.Controls.Add(this.lbl_room);
            this.Controls.Add(this.tlp_playersArea);
            this.Controls.Add(this.tlp_parameters);
            this.Controls.Add(this.btn_leaveLobby);
            this.Controls.Add(this.btn_startGame);
            this.Controls.Add(this.pictureBox2);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximumSize = new System.Drawing.Size(1285, 730);
            this.MinimumSize = new System.Drawing.Size(1285, 730);
            this.Name = "Lobby";
            this.Text = "Lobby";
            this.tlp_parameters.ResumeLayout(false);
            this.tlp_parameters.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tlp_playersArea.ResumeLayout(false);
            this.tlp_playersArea.PerformLayout();
            this.tlp_players.ResumeLayout(false);
            this.tlp_players.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btn_leaveLobby;
        private System.Windows.Forms.Button btn_startGame;
        private ContextMenuStrip contextMenuStrip1;
        private ContextMenuStrip contextMenuStrip2;
        private TableLayoutPanel tlp_parameters;
        private Label lbl_parameters;
        private TableLayoutPanel tlp_playersArea;
        private Label lbl_players;
        private Label lbl_room;
        private TableLayoutPanel tableLayoutPanel1;
        private Label lbl_impostorNumberValue;
        private Label lbl_imposterNumber;
        private Label lbl_timeToWrite;
        private Label lbl_timeToVote;
        private Label lbl_turnMaxValue;
        private Label lbl_timeToWriteValue;
        private Label lbl_timeToVoteValue;
        private Label lbl_turnMax;
        private TableLayoutPanel tlp_players;
        private Label lbl_playerNumber;
        private Label lbl_playerNumberWarning;
        private PictureBox pictureBox2;
    }
}