﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UndercoverGame.Service;
using UndercoverGame.View;
namespace UndercoverGame
{
    public partial class Lobby : ViewTemplate
    {
        private List<Label> _playerLabels = new List<Label>();
        private List<User> _players = new List<User>();

        public Lobby()
        {
            InitializeComponent();
            InitializeLobby();
            InitializeViewWithTheme();
        }

        #region Initialization
        private void InitializeLobby()
        {
            InitializeStartButton();
            InitializePlayerLabels();
            SetPlayers();
            this.lbl_room.Text = LobbySocketService._instance.IdRoom;
            LobbySocketService._instance.ListenPlayersUpdates(() => this.Invoke(new MethodInvoker(SetPlayers)));
            LobbySocketService._instance.ListenStartGame(() => this.Invoke(new MethodInvoker(GoToGame)));
            InitializeParameters();
        }

        private void InitializeParameters()
        {
            lbl_impostorNumberValue.Text = LobbySocketService._instance.Parameters.ImpostorNumber.ToString();
            lbl_turnMaxValue.Text = LobbySocketService._instance.Parameters.TurnMax.ToString();
            lbl_timeToWriteValue.Text = LobbySocketService._instance.Parameters.TimeToWrite.ToString();
            lbl_timeToVoteValue.Text = LobbySocketService._instance.Parameters.TimeToVote.ToString();
        }

        private void InitializeStartButton()
        {
            if (AuthService._instance.User.Id == LobbySocketService._instance.Players.First().Id)
            {
                btn_startGame.Visible = true;
            }
        }

        private void InitializePlayerLabels()
        {
            for (int i = 0; i < 10; i++)
            {
                _playerLabels.Add(CreateLabel("player_" + i, i.ToString()));
                tlp_players.Controls.Add(_playerLabels[i], 0, i + 1);
            }
        }
        public void InitializeViewWithTheme()
        {
            //Background
            this.BackColor = BackgndColor;

            //Text 
            lbl_imposterNumber.ForeColor = lbl_impostorNumberValue.ForeColor = lbl_parameters.ForeColor = lbl_playerNumber.ForeColor =
               lbl_players.ForeColor = lbl_room.ForeColor = lbl_timeToVote.ForeColor = lbl_timeToVoteValue.ForeColor = lbl_timeToWrite.ForeColor =
               lbl_timeToWriteValue.ForeColor = lbl_turnMax.ForeColor = lbl_turnMaxValue.ForeColor = TextColor;

            //Text error
            lbl_playerNumberWarning.ForeColor = ErrorColor;

            //Button
            btn_leaveLobby.BackColor = btn_startGame.BackColor = ButtonColor;

            //Text Button
            btn_startGame.ForeColor = btn_leaveLobby.ForeColor = TextButtonColor;
        }
        #endregion
        private void SetPlayers()
        {
            lbl_playerNumber.Text = LobbySocketService._instance.Players.Count.ToString() + " / 10";
            List<User> uncheckedPlayers = new List<User>();

            int playerIndex = 0;
            LobbySocketService._instance.Players.ForEach(player =>            
            {
                if (_players.Contains(player))
                {
                    uncheckedPlayers.Remove(player);
                }
                else
                {
                    SetPlayerLabel(playerIndex, player.Username);
                }
                playerIndex++;

                if (playerIndex > 3)
                {
                    btn_startGame.Enabled = true;
                    lbl_playerNumberWarning.Visible = false;
                }
                else if (playerIndex > 1)
                {
                    btn_startGame.Enabled = false;
                    lbl_playerNumberWarning.Visible = true;
                    InitializeStartButton();
                }
            });

            for (; playerIndex < 10; playerIndex++)
            {
                SetPlayerLabel(playerIndex);
            }
        }

        private void SetPlayerLabel(int playerIndex, string username = "")
        {
            _playerLabels[playerIndex].Text = username;
        }

        private Label CreateLabel(string name, string text)
        {
            Label lbl = new Label();
            lbl.Anchor = ((AnchorStyles)((((AnchorStyles.Top | AnchorStyles.Bottom) | AnchorStyles.Left) | AnchorStyles.Right)));
            lbl.AutoSize = true;
            lbl.FlatStyle = FlatStyle.Flat;
            lbl.Font = new Font("Microsoft Sans Serif",15F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0)));
            lbl.ForeColor = TextColor;
            lbl.Location = new Point(285, 0);
            lbl.Name = "lbl_" + name;
            lbl.Size = new Size(277, 58);
            lbl.TabIndex = 9;
            lbl.Text = text;
            lbl.TextAlign = ContentAlignment.MiddleCenter;

            return lbl;
        }
        #region Front Events
        private void btn_leaveLobby_Click(object sender, EventArgs e)
        {
            LobbySocketService._instance.LeaveLobby();
            Form playForm = new Play();
            playForm.StartPosition = FormStartPosition.Manual;
            playForm.Left = this.Left;
            playForm.Top = this.Top;
            this.Visible = false;
            playForm.Show();
        }

        private void btn_startGame_Click(object sender, EventArgs e)
        {
            LobbySocketService._instance.StartGame();
        }
        #endregion

        private void GoToGame()
        {
            Form gameForm = new Game();
            gameForm.StartPosition = FormStartPosition.Manual;
            gameForm.Left = this.Left;
            gameForm.Top = this.Top;
            this.Visible = false;
            gameForm.Show();
        }
    }
}
