﻿using System.Drawing;
using System.Windows.Forms;
using UndercoverGame.Service;

namespace UndercoverGame.View
{
    public partial class ViewTemplate : Form
    {
        public Color BackgndColor { get; set; }
        public Color TextColor { get; set; }
        public Color ButtonColor { get; set; }
        public Color ErrorColor { get; set; }
        public Color TextButtonColor { get; set; }
        public Theme ThemeSelected { get; set; }
        public enum Theme
        {
            CLASSIC, 
            BLOODY,
            MUSTARD,
            WINE,
            SKY
        }
        public ViewTemplate()
        {
            InitializeComponent();
            InitializeTheme();
        }       
        public void InitializeTheme()
        {
            Theme themeSelected;
            try
            {
                Theme.TryParse(XMLService._instance.GetTheme().ToUpper(), out themeSelected);
                switch (themeSelected)
                {
                    case Theme.CLASSIC:
                        ThemeSelected = themeSelected;
                        break;
                    case Theme.BLOODY:
                        if (AuthService._instance.User.BloodyTheme == true) ThemeSelected = themeSelected;
                        else ThemeSelected = Theme.CLASSIC;
                        break;
                    case Theme.MUSTARD:
                        if (AuthService._instance.User.MustardTheme == true) ThemeSelected = themeSelected;
                        else ThemeSelected = Theme.CLASSIC;
                        break;
                    case Theme.WINE:
                        if (AuthService._instance.User.WineTheme == true) ThemeSelected = themeSelected;
                        else ThemeSelected = Theme.CLASSIC;
                        break;
                    case Theme.SKY:
                        if (AuthService._instance.User.SkyTheme == true) ThemeSelected = themeSelected;
                        else ThemeSelected = Theme.CLASSIC;
                        break;
                    default:
                        ThemeSelected = Theme.CLASSIC;
                        break;
                }
            }
            catch
            {
                ThemeSelected = Theme.CLASSIC;
            }
            InitializeColorTheme();
        }
        public void InitializeColorTheme()
        {
            switch (ThemeSelected)
            {
                case Theme.CLASSIC:
                    BackgndColor = Color.FromArgb(50, 53, 59);
                    TextColor = Color.White;
                    ButtonColor = Color.FromArgb(50, 53, 58);
                    ErrorColor = Color.Brown;
                    TextButtonColor = Color.White;
                    break;
                case Theme.BLOODY:
                    BackgndColor = Color.FromArgb(109, 22, 22);
                    TextColor = Color.White;
                    ButtonColor = Color.FromArgb(47, 2, 3);
                    ErrorColor = Color.Red;
                    TextButtonColor = Color.White;
                    break;
                case Theme.MUSTARD:
                    BackgndColor = Color.FromArgb(235, 206, 1);
                    TextColor = Color.Black;
                    ButtonColor = Color.FromArgb(217, 136, 38);
                    ErrorColor = Color.Brown;
                    TextButtonColor = Color.White;
                    break;
                case Theme.WINE:
                    BackgndColor = Color.FromArgb(81, 0, 81);
                    TextColor = Color.White;
                    ButtonColor = Color.FromArgb(160, 203, 177);
                    ErrorColor = Color.IndianRed;
                    TextButtonColor = Color.White;
                    break;
                case Theme.SKY:
                    BackgndColor = Color.FromArgb(204, 255, 255);
                    TextColor = Color.Black;
                    ButtonColor = Color.White;
                    ErrorColor = Color.Firebrick;
                    TextButtonColor = Color.Black;
                    break;
                default:
                    break;
            }
        }
        private void ViewTemplate_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
    }
}
