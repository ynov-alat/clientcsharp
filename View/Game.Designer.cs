﻿namespace UndercoverGame.View
{
    partial class Game
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Game));
            this.tlp_votePlayers = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_timer = new System.Windows.Forms.Label();
            this.pb_timer = new System.Windows.Forms.PictureBox();
            this.lbl_word = new System.Windows.Forms.Label();
            this.tlp_players = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_wordError = new System.Windows.Forms.Label();
            this.btn_sendVote = new System.Windows.Forms.Button();
            this.btn_sendWord = new System.Windows.Forms.Button();
            this.lbl_WriteWord = new System.Windows.Forms.Label();
            this.tb_wordPlayer = new System.Windows.Forms.TextBox();
            this.tlp_Words = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_roomName = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pb_timer)).BeginInit();
            this.SuspendLayout();
            // 
            // tlp_votePlayers
            // 
            this.tlp_votePlayers.BackColor = System.Drawing.Color.Transparent;
            this.tlp_votePlayers.ColumnCount = 1;
            this.tlp_votePlayers.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlp_votePlayers.Location = new System.Drawing.Point(12, 144);
            this.tlp_votePlayers.Name = "tlp_votePlayers";
            this.tlp_votePlayers.RowCount = 1;
            this.tlp_votePlayers.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlp_votePlayers.Size = new System.Drawing.Size(867, 148);
            this.tlp_votePlayers.TabIndex = 14;
            this.tlp_votePlayers.Visible = false;
            // 
            // lbl_timer
            // 
            this.lbl_timer.AutoSize = true;
            this.lbl_timer.Font = new System.Drawing.Font("Calisto MT", 25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_timer.ForeColor = System.Drawing.Color.White;
            this.lbl_timer.Location = new System.Drawing.Point(1069, 144);
            this.lbl_timer.Name = "lbl_timer";
            this.lbl_timer.Size = new System.Drawing.Size(77, 39);
            this.lbl_timer.TabIndex = 13;
            this.lbl_timer.Text = "-----";
            // 
            // pb_timer
            // 
            this.pb_timer.Image = global::UndercoverGame.Properties.Resources.timer;
            this.pb_timer.InitialImage = global::UndercoverGame.Properties.Resources.timer;
            this.pb_timer.Location = new System.Drawing.Point(1005, 134);
            this.pb_timer.Name = "pb_timer";
            this.pb_timer.Size = new System.Drawing.Size(56, 58);
            this.pb_timer.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_timer.TabIndex = 12;
            this.pb_timer.TabStop = false;
            // 
            // lbl_word
            // 
            this.lbl_word.AutoSize = true;
            this.lbl_word.Font = new System.Drawing.Font("Calisto MT", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_word.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lbl_word.Location = new System.Drawing.Point(552, 21);
            this.lbl_word.Name = "lbl_word";
            this.lbl_word.Size = new System.Drawing.Size(67, 39);
            this.lbl_word.TabIndex = 11;
            this.lbl_word.Text = "-----";
            // 
            // tlp_players
            // 
            this.tlp_players.ColumnCount = 1;
            this.tlp_players.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlp_players.Cursor = System.Windows.Forms.Cursors.Hand;
            this.tlp_players.Location = new System.Drawing.Point(12, 63);
            this.tlp_players.Name = "tlp_players";
            this.tlp_players.RowCount = 1;
            this.tlp_players.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlp_players.Size = new System.Drawing.Size(867, 58);
            this.tlp_players.TabIndex = 9;
            // 
            // lbl_wordError
            // 
            this.lbl_wordError.AutoSize = true;
            this.lbl_wordError.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_wordError.ForeColor = System.Drawing.Color.Brown;
            this.lbl_wordError.Location = new System.Drawing.Point(1000, 305);
            this.lbl_wordError.Name = "lbl_wordError";
            this.lbl_wordError.Size = new System.Drawing.Size(180, 25);
            this.lbl_wordError.TabIndex = 8;
            this.lbl_wordError.Text = "Please write a word";
            this.lbl_wordError.Visible = false;
            // 
            // btn_sendVote
            // 
            this.btn_sendVote.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btn_sendVote.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(53)))), ((int)(((byte)(58)))));
            this.btn_sendVote.Enabled = false;
            this.btn_sendVote.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_sendVote.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_sendVote.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_sendVote.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_sendVote.Location = new System.Drawing.Point(1019, 493);
            this.btn_sendVote.Name = "btn_sendVote";
            this.btn_sendVote.Size = new System.Drawing.Size(170, 46);
            this.btn_sendVote.TabIndex = 7;
            this.btn_sendVote.Text = "Send Vote";
            this.btn_sendVote.UseVisualStyleBackColor = false;
            this.btn_sendVote.Click += new System.EventHandler(this.btn_sendVote_Click);
            // 
            // btn_sendWord
            // 
            this.btn_sendWord.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btn_sendWord.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(53)))), ((int)(((byte)(58)))));
            this.btn_sendWord.Enabled = false;
            this.btn_sendWord.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_sendWord.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_sendWord.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_sendWord.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_sendWord.Location = new System.Drawing.Point(1047, 344);
            this.btn_sendWord.Name = "btn_sendWord";
            this.btn_sendWord.Size = new System.Drawing.Size(99, 46);
            this.btn_sendWord.TabIndex = 6;
            this.btn_sendWord.Text = "Send";
            this.btn_sendWord.UseVisualStyleBackColor = false;
            this.btn_sendWord.Click += new System.EventHandler(this.btn_sendWord_Click);
            // 
            // lbl_WriteWord
            // 
            this.lbl_WriteWord.AutoSize = true;
            this.lbl_WriteWord.Font = new System.Drawing.Font("Calisto MT", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_WriteWord.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lbl_WriteWord.Location = new System.Drawing.Point(1000, 216);
            this.lbl_WriteWord.Name = "lbl_WriteWord";
            this.lbl_WriteWord.Size = new System.Drawing.Size(183, 28);
            this.lbl_WriteWord.TabIndex = 4;
            this.lbl_WriteWord.Text = "Write your word";
            // 
            // tb_wordPlayer
            // 
            this.tb_wordPlayer.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_wordPlayer.Location = new System.Drawing.Point(994, 257);
            this.tb_wordPlayer.Name = "tb_wordPlayer";
            this.tb_wordPlayer.Size = new System.Drawing.Size(195, 35);
            this.tb_wordPlayer.TabIndex = 2;
            // 
            // tlp_Words
            // 
            this.tlp_Words.ColumnCount = 1;
            this.tlp_Words.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlp_Words.Location = new System.Drawing.Point(12, 144);
            this.tlp_Words.Name = "tlp_Words";
            this.tlp_Words.RowCount = 1;
            this.tlp_Words.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlp_Words.Size = new System.Drawing.Size(867, 534);
            this.tlp_Words.TabIndex = 1;
            // 
            // lbl_roomName
            // 
            this.lbl_roomName.AutoSize = true;
            this.lbl_roomName.Font = new System.Drawing.Font("Calisto MT", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_roomName.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lbl_roomName.Location = new System.Drawing.Point(12, 23);
            this.lbl_roomName.Name = "lbl_roomName";
            this.lbl_roomName.Size = new System.Drawing.Size(124, 23);
            this.lbl_roomName.TabIndex = 0;
            this.lbl_roomName.Text = "Name room :";
            // 
            // Game
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(53)))), ((int)(((byte)(59)))));
            this.ClientSize = new System.Drawing.Size(1268, 690);
            this.Controls.Add(this.tlp_votePlayers);
            this.Controls.Add(this.lbl_timer);
            this.Controls.Add(this.pb_timer);
            this.Controls.Add(this.lbl_word);
            this.Controls.Add(this.tlp_players);
            this.Controls.Add(this.lbl_wordError);
            this.Controls.Add(this.btn_sendVote);
            this.Controls.Add(this.btn_sendWord);
            this.Controls.Add(this.lbl_WriteWord);
            this.Controls.Add(this.tb_wordPlayer);
            this.Controls.Add(this.tlp_Words);
            this.Controls.Add(this.lbl_roomName);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Game";
            this.Text = "Game";
            ((System.ComponentModel.ISupportInitialize)(this.pb_timer)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_roomName;
        private System.Windows.Forms.TableLayoutPanel tlp_Words;
        private System.Windows.Forms.TextBox tb_wordPlayer;
        private System.Windows.Forms.Label lbl_WriteWord;
        private System.Windows.Forms.Button btn_sendWord;
        private System.Windows.Forms.Button btn_sendVote;
        private System.Windows.Forms.Label lbl_wordError;
        private System.Windows.Forms.TableLayoutPanel tlp_players;
        private System.Windows.Forms.Label lbl_word;
        private System.Windows.Forms.PictureBox pb_timer;
        private System.Windows.Forms.Label lbl_timer;
        private System.Windows.Forms.TableLayoutPanel tlp_votePlayers;
    }
}