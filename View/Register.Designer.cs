﻿namespace UndercoverGame.View
{
    partial class Register
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Register));
            this.label1 = new System.Windows.Forms.Label();
            this.lbl_registration = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lbl_email = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.TPL_registration = new System.Windows.Forms.TableLayoutPanel();
            this.tb_password = new System.Windows.Forms.TextBox();
            this.tb_email = new System.Windows.Forms.TextBox();
            this.tb_username = new System.Windows.Forms.TextBox();
            this.lbl_usernameError = new System.Windows.Forms.Label();
            this.btn_register = new System.Windows.Forms.Button();
            this.lbl_fieldsRequired = new System.Windows.Forms.Label();
            this.lbl_login = new System.Windows.Forms.Label();
            this.btn_login = new System.Windows.Forms.Button();
            this.lbl_passwordError = new System.Windows.Forms.Label();
            this.lbl_emailError = new System.Windows.Forms.Label();
            this.lbl_loginError = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.TPL_registration.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(1540, 100);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Registration";
            // 
            // lbl_registration
            // 
            this.lbl_registration.AutoSize = true;
            this.lbl_registration.Font = new System.Drawing.Font("Calisto MT", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_registration.ForeColor = System.Drawing.Color.White;
            this.lbl_registration.Location = new System.Drawing.Point(982, 190);
            this.lbl_registration.Name = "lbl_registration";
            this.lbl_registration.Size = new System.Drawing.Size(153, 31);
            this.lbl_registration.TabIndex = 1;
            this.lbl_registration.Text = "Registration";
            this.lbl_registration.Click += new System.EventHandler(this.lbl_registration_Click);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calisto MT", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(3, 138);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(221, 26);
            this.label3.TabIndex = 2;
            this.label3.Text = "Username :";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_email
            // 
            this.lbl_email.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_email.AutoSize = true;
            this.lbl_email.Font = new System.Drawing.Font("Calisto MT", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_email.ForeColor = System.Drawing.Color.White;
            this.lbl_email.Location = new System.Drawing.Point(3, 56);
            this.lbl_email.Name = "lbl_email";
            this.lbl_email.Size = new System.Drawing.Size(221, 26);
            this.lbl_email.TabIndex = 3;
            this.lbl_email.Text = "Email :";
            this.lbl_email.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_email.Visible = false;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calisto MT", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(3, 222);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(221, 26);
            this.label5.TabIndex = 4;
            this.label5.Text = "Password :";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // TPL_registration
            // 
            this.TPL_registration.ColumnCount = 2;
            this.TPL_registration.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.TPL_registration.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.00001F));
            this.TPL_registration.Controls.Add(this.tb_password, 1, 2);
            this.TPL_registration.Controls.Add(this.label5, 0, 2);
            this.TPL_registration.Controls.Add(this.label3, 0, 1);
            this.TPL_registration.Controls.Add(this.lbl_email, 0, 0);
            this.TPL_registration.Controls.Add(this.tb_email, 1, 0);
            this.TPL_registration.Controls.Add(this.tb_username, 1, 1);
            this.TPL_registration.Location = new System.Drawing.Point(677, 242);
            this.TPL_registration.Name = "TPL_registration";
            this.TPL_registration.RowCount = 3;
            this.TPL_registration.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.TPL_registration.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.TPL_registration.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.TPL_registration.Size = new System.Drawing.Size(456, 248);
            this.TPL_registration.TabIndex = 6;
            // 
            // tb_password
            // 
            this.tb_password.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tb_password.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_password.Location = new System.Drawing.Point(230, 215);
            this.tb_password.Name = "tb_password";
            this.tb_password.Size = new System.Drawing.Size(223, 30);
            this.tb_password.TabIndex = 7;
            this.tb_password.UseSystemPasswordChar = true;
            // 
            // tb_email
            // 
            this.tb_email.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tb_email.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_email.Location = new System.Drawing.Point(230, 49);
            this.tb_email.Name = "tb_email";
            this.tb_email.Size = new System.Drawing.Size(223, 30);
            this.tb_email.TabIndex = 5;
            this.tb_email.Visible = false;
            // 
            // tb_username
            // 
            this.tb_username.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tb_username.BackColor = System.Drawing.SystemColors.Window;
            this.tb_username.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_username.Location = new System.Drawing.Point(230, 131);
            this.tb_username.Name = "tb_username";
            this.tb_username.Size = new System.Drawing.Size(223, 30);
            this.tb_username.TabIndex = 6;
            // 
            // lbl_usernameError
            // 
            this.lbl_usernameError.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_usernameError.AutoSize = true;
            this.lbl_usernameError.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_usernameError.ForeColor = System.Drawing.Color.IndianRed;
            this.lbl_usernameError.Location = new System.Drawing.Point(625, 526);
            this.lbl_usernameError.Name = "lbl_usernameError";
            this.lbl_usernameError.Size = new System.Drawing.Size(590, 18);
            this.lbl_usernameError.TabIndex = 8;
            this.lbl_usernameError.Text = "The username must have a size between 3 and 16 characters without special charact" +
    "ers.";
            this.lbl_usernameError.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_usernameError.Visible = false;
            // 
            // btn_register
            // 
            this.btn_register.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(53)))), ((int)(((byte)(59)))));
            this.btn_register.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_register.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_register.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_register.Location = new System.Drawing.Point(834, 627);
            this.btn_register.Name = "btn_register";
            this.btn_register.Size = new System.Drawing.Size(166, 52);
            this.btn_register.TabIndex = 8;
            this.btn_register.Text = "Register";
            this.btn_register.UseVisualStyleBackColor = false;
            this.btn_register.Visible = false;
            this.btn_register.Click += new System.EventHandler(this.btn_register_Click);
            // 
            // lbl_fieldsRequired
            // 
            this.lbl_fieldsRequired.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_fieldsRequired.AutoSize = true;
            this.lbl_fieldsRequired.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_fieldsRequired.ForeColor = System.Drawing.Color.IndianRed;
            this.lbl_fieldsRequired.Location = new System.Drawing.Point(804, 577);
            this.lbl_fieldsRequired.Name = "lbl_fieldsRequired";
            this.lbl_fieldsRequired.Size = new System.Drawing.Size(184, 22);
            this.lbl_fieldsRequired.TabIndex = 9;
            this.lbl_fieldsRequired.Text = "All fields are required.";
            this.lbl_fieldsRequired.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_fieldsRequired.Visible = false;
            // 
            // lbl_login
            // 
            this.lbl_login.AutoSize = true;
            this.lbl_login.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_login.Font = new System.Drawing.Font("Calisto MT", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_login.ForeColor = System.Drawing.Color.White;
            this.lbl_login.Location = new System.Drawing.Point(707, 188);
            this.lbl_login.Name = "lbl_login";
            this.lbl_login.Size = new System.Drawing.Size(85, 33);
            this.lbl_login.TabIndex = 10;
            this.lbl_login.Text = "Login";
            this.lbl_login.Click += new System.EventHandler(this.lbl_login_Click);
            // 
            // btn_login
            // 
            this.btn_login.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(53)))), ((int)(((byte)(59)))));
            this.btn_login.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_login.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_login.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_login.Location = new System.Drawing.Point(834, 627);
            this.btn_login.Name = "btn_login";
            this.btn_login.Size = new System.Drawing.Size(135, 52);
            this.btn_login.TabIndex = 11;
            this.btn_login.Text = "Login";
            this.btn_login.UseVisualStyleBackColor = false;
            this.btn_login.Click += new System.EventHandler(this.btn_login_Click);
            // 
            // lbl_passwordError
            // 
            this.lbl_passwordError.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_passwordError.AutoSize = true;
            this.lbl_passwordError.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_passwordError.ForeColor = System.Drawing.Color.IndianRed;
            this.lbl_passwordError.Location = new System.Drawing.Point(683, 559);
            this.lbl_passwordError.Name = "lbl_passwordError";
            this.lbl_passwordError.Size = new System.Drawing.Size(429, 18);
            this.lbl_passwordError.TabIndex = 10;
            this.lbl_passwordError.Text = "Password must contains at least 8 characters and 1 lower case.";
            this.lbl_passwordError.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_passwordError.Visible = false;
            // 
            // lbl_emailError
            // 
            this.lbl_emailError.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_emailError.AutoSize = true;
            this.lbl_emailError.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_emailError.ForeColor = System.Drawing.Color.IndianRed;
            this.lbl_emailError.Location = new System.Drawing.Point(843, 543);
            this.lbl_emailError.Name = "lbl_emailError";
            this.lbl_emailError.Size = new System.Drawing.Size(109, 18);
            this.lbl_emailError.TabIndex = 9;
            this.lbl_emailError.Text = "Incorrect email.";
            this.lbl_emailError.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_emailError.Visible = false;
            // 
            // lbl_loginError
            // 
            this.lbl_loginError.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_loginError.AutoSize = true;
            this.lbl_loginError.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_loginError.ForeColor = System.Drawing.Color.IndianRed;
            this.lbl_loginError.Location = new System.Drawing.Point(845, 526);
            this.lbl_loginError.Name = "lbl_loginError";
            this.lbl_loginError.Size = new System.Drawing.Size(180, 18);
            this.lbl_loginError.TabIndex = 12;
            this.lbl_loginError.Text = "Email/password incorrect.";
            this.lbl_loginError.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_loginError.Visible = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::UndercoverGame.Properties.Resources.detective;
            this.pictureBox2.Location = new System.Drawing.Point(-69, 66);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(771, 684);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 16;
            this.pictureBox2.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Calisto MT", 28F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(830, 137);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(192, 44);
            this.label4.TabIndex = 18;
            this.label4.Text = "The Game";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Calisto MT", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(702, 66);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(428, 62);
            this.label6.TabIndex = 17;
            this.label6.Text = "UNDERCOVER";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Register
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(53)))), ((int)(((byte)(63)))));
            this.ClientSize = new System.Drawing.Size(1268, 690);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lbl_registration);
            this.Controls.Add(this.lbl_loginError);
            this.Controls.Add(this.lbl_usernameError);
            this.Controls.Add(this.lbl_emailError);
            this.Controls.Add(this.btn_login);
            this.Controls.Add(this.lbl_login);
            this.Controls.Add(this.lbl_fieldsRequired);
            this.Controls.Add(this.btn_register);
            this.Controls.Add(this.TPL_registration);
            this.Controls.Add(this.lbl_passwordError);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox2);
            this.ForeColor = System.Drawing.Color.White;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Register";
            this.Text = "UndercoverGame Authentication";
            this.TPL_registration.ResumeLayout(false);
            this.TPL_registration.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbl_registration;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lbl_email;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TableLayoutPanel TPL_registration;
        private System.Windows.Forms.TextBox tb_password;
        private System.Windows.Forms.TextBox tb_email;
        private System.Windows.Forms.TextBox tb_username;
        private System.Windows.Forms.Button btn_register;
        private System.Windows.Forms.Label lbl_usernameError;
        private System.Windows.Forms.Label lbl_fieldsRequired;
        private System.Windows.Forms.Label lbl_login;
        private System.Windows.Forms.Button btn_login;
        private System.Windows.Forms.Label lbl_passwordError;
        private System.Windows.Forms.Label lbl_emailError;
        private System.Windows.Forms.Label lbl_loginError;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
    }
}