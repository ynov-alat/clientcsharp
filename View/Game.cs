﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UndercoverGame.Service;
using UndercoverGame.Model;

namespace UndercoverGame.View
{
    public partial class Game : ViewTemplate
    {
        private int _rowPosition;
        private int _columnPosition;
        private Parameters _parameters;
        private List<User> _players;
        private List<string> _wordsPlayers;
        private Timer _timer;
        private int _counter;
        private User _playerVoted;
        private int _turnNumber;
        private bool _isTimeToVote;
        private bool _isPlayerTurn;
        private enum ENamesTLP
        {
            USERNAME,
            WORD,
            VOTE
        }

        public Game()
        {
            InitializeComponent();
            IntializeVariables();
            InitializeListeningSocket();
            IntializeNameRoom();
            InitializeViewWithTheme();
        }
        #region Inititialization
        private void IntializeVariables()
        {
            _rowPosition = 0;
            _columnPosition = 0;
            _parameters = new Parameters();
            _wordsPlayers = new List<string>();
            _timer = new Timer();
            _counter = _parameters.TimeToWrite;//30
            _playerVoted = null;
            _turnNumber = 0;
            _isTimeToVote = false;
            _timer.Tick += new EventHandler(UpdateTimer);
            _isPlayerTurn = false;
        }
        private void IntializeNameRoom()
        {
            lbl_roomName.Text += LobbySocketService._instance.IdRoom;
        }
        private void InitializeWordPlayer(string word)
        {
            lbl_word.Text = word;
        }
        private void IntializeTableLayoutPanels()
        {
            _players = LobbySocketService._instance.Players;

            tlp_Words.RowCount = _parameters.TurnMax;
            tlp_Words.ColumnCount = tlp_players.ColumnCount = tlp_votePlayers.RowCount = tlp_votePlayers.ColumnCount = _players.Count();

            for (int x = 0; x < tlp_Words.RowCount; x++)
            {
                if (x == 0) tlp_Words.RowStyles.RemoveAt(0);
                tlp_Words.RowStyles.Add(new RowStyle(SizeType.Percent, 100 / _parameters.TurnMax));
            }
            for (int x = 0; x < tlp_Words.ColumnCount; x++)
            {
                if (x == 0)
                {
                    tlp_Words.ColumnStyles.RemoveAt(0);
                    tlp_players.ColumnStyles.RemoveAt(0);
                    tlp_votePlayers.ColumnStyles.RemoveAt(0);
                    tlp_votePlayers.RowStyles.RemoveAt(0);
                }
                tlp_Words.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100 / _players.Count()));
                tlp_players.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100 / _players.Count()));
                tlp_votePlayers.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100 / _players.Count()));
                tlp_votePlayers.RowStyles.Add(new RowStyle(SizeType.Percent, 100 / _players.Count()));

            }

            foreach (User player in _players)
            {
                AddLabelToTableLayoutPanels(player.Username, ENamesTLP.USERNAME);
            }
        }
        public void InitializeViewWithTheme()
        {
            //Background
            this.BackColor = BackgndColor;

            //Text 
            lbl_roomName.ForeColor = lbl_timer.ForeColor = lbl_WriteWord.ForeColor = lbl_word.ForeColor = TextColor;

            //Text error
            lbl_wordError.ForeColor = ErrorColor;

            //Button
            btn_sendVote.BackColor = btn_sendWord.BackColor = ButtonColor;

            //Text Button
            btn_sendVote.ForeColor = btn_sendWord.ForeColor = TextButtonColor;
        }
        #endregion     
        #region Socket
        private void InitializeListeningSocket()
        {
            LobbySocketService._instance.ListenDistributionWords(word => this.Invoke(new MethodInvoker((() => this.InitializeWordPlayer(word)))));
            LobbySocketService._instance.ListenOrderPlayers(() => this.Invoke(new MethodInvoker((() => this.IntializeTableLayoutPanels()))));
            LobbySocketService._instance.ListenWordsFromPlayers(response => this.Invoke(new MethodInvoker((() => this.HandleWordsFromPlayersSocketResponse(response)))));
            LobbySocketService._instance.ListenVoteTurn(word => this.Invoke(new MethodInvoker((() => this.TimeToVote(word)))));
            LobbySocketService._instance.ListenPlayerVote(response => this.Invoke(new MethodInvoker((() => this.HandleVoteFromPlayers(response)))));
            LobbySocketService._instance.ListenResults(() => this.Invoke(new MethodInvoker((() => this.DisplayResultGame()))));
        }
        private void SendWordToServer(string word)
        {
            tb_wordPlayer.Text = "";
            if (!_isTimeToVote) LobbySocketService._instance.SendWord(word);
            else LobbySocketService._instance.SendMrWhiteWord(word);
        }
        private void HandleWordsFromPlayersSocketResponse(string[] response)
        {
            AddLabelToTableLayoutPanels(response[0], ENamesTLP.WORD);
            SetPlayersTurn(response[1]);
            _turnNumber = int.Parse(response[2]);
        }
        private void HandleVoteFromPlayers(Tuple<string, string> response)
        {
            AddLabelToTableLayoutPanels(response.Item1, ENamesTLP.VOTE, response.Item2);
        }
        private void SendVotePlayerToServer(string idUser)
        {
            LobbySocketService._instance.SendVote(idUser);
        }
        #endregion
        #region Private Methods
        private void StartTimer()
        {
            _timer.Interval = (1000);
            lbl_timer.ForeColor = TextColor;
            _counter = _parameters.TimeToWrite;
            _timer.Start();

        }
        private void UpdateTimer(object sender, EventArgs e)
        {
            if (_isPlayerTurn)
            {
                _counter--;
                lbl_timer.Text = _counter.ToString(); ;
                if (_counter == 0)
                {
                    if (_isTimeToVote)
                    {
                        btn_sendVote.Visible = false;
                    }
                    _counter = _parameters.TimeToWrite;
                    _isPlayerTurn = false;
                    lbl_timer.Text = "-----";
                    lbl_timer.ForeColor = ErrorColor;
                    btn_sendWord.Enabled = false;
                }
            }

        }
        private void SetPlayersTurn(string idPlayer)
        {
            btn_sendWord.Enabled = idPlayer == XMLService._instance.GetUser().Id ? true : false;
            _isPlayerTurn = true;
            StartTimer();
        }
        private void AddLabelToTableLayoutPanels(string value, ENamesTLP tlp, string userGetVote = "")
        {
            Label lbl = new Label();
            lbl.Anchor = ((AnchorStyles)((((AnchorStyles.Top | AnchorStyles.Bottom) | AnchorStyles.Left) | AnchorStyles.Right)));
            lbl.AutoSize = true;
            lbl.FlatStyle = FlatStyle.Flat;
            lbl.Location = new Point(285, 0);
            lbl.Name = "lbl_" + value;
            lbl.Size = new Size(277, 58);
            lbl.TabIndex = 9;
            lbl.TextAlign = ContentAlignment.MiddleCenter;
            if (tlp == ENamesTLP.WORD && _rowPosition < _parameters.TurnMax && value != string.Empty)
            {
                lbl.Font = new Font("Microsoft Sans Serif", 15F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0)));
                lbl.ForeColor = TextColor;
                lbl.Text = value;
                tlp_Words.Controls.Add(lbl, _columnPosition, _rowPosition);
                _wordsPlayers.Add(value);
            }
            else if (tlp == ENamesTLP.USERNAME)
            {

                lbl.Font = new Font("Microsoft Sans Serif", 15F, FontStyle.Bold, GraphicsUnit.Point, ((byte)(0)));
                if (AuthService._instance.User.Username == value) lbl.BackColor = Color.PeachPuff;
                else lbl.BackColor = Color.LightGray;

                lbl.ForeColor = TextButtonColor;
                lbl.Text = value;
                lbl.Click += new EventHandler(lbl_players_Click);
                tlp_players.Controls.Add(lbl, _columnPosition, _rowPosition);
            }
            else if (tlp == ENamesTLP.VOTE)
            {
                lbl.Font = new Font("Microsoft Sans Serif", 15F, FontStyle.Bold, GraphicsUnit.Point, ((byte)(0)));
                lbl.ForeColor = TextColor;
                lbl.Text = _players.Find(x => x.Id == value).Username;
                int column = _players.FindIndex(x => x.Id == userGetVote);
                for (int j = 0; j <= this.tlp_votePlayers.RowCount; j++)
                {
                    Control c = this.tlp_votePlayers.GetControlFromPosition(column, j);
                    if (c == null)
                    {
                        tlp_votePlayers.Controls.Add(lbl, column, j);
                        break;
                    }
                }
                return;
            }

            if (_columnPosition == (_players.Count() - 1))
            {
                _columnPosition = 0;
                if (tlp == ENamesTLP.WORD) _rowPosition++;
                else _rowPosition = 0;
            }
            else if (value != string.Empty) _columnPosition++;
        }
        private void TimeToVote(string lastWord)
        {
            AddLabelToTableLayoutPanels(lastWord, ENamesTLP.WORD);
            _isTimeToVote = true;
            btn_sendVote.Visible = true;
            _counter = _parameters.TimeToVote;
            tlp_votePlayers.Visible = true;
            tlp_Words.Location = new Point(12, 305);
            tlp_Words.Size = new Size(867, 373);
            _isPlayerTurn = true;

            if (lbl_word.Text == string.Empty)//Mr White
            {
                lbl_WriteWord.Text += ", Mr White.";
                lbl_WriteWord.Location = new Point(950, 214);
                btn_sendWord.Enabled = true;
            }
            else
            {
                lbl_WriteWord.Visible = false;
                tb_wordPlayer.Visible = false;
                btn_sendWord.Visible = false;
            }

        }
        private void DisplayResultGame()
        {
            Form resultForm = new Result();
            resultForm.StartPosition = FormStartPosition.Manual;
            resultForm.Left = this.Left;
            resultForm.Top = this.Top;
            this.Visible = false;
            resultForm.Show();
        }
        #endregion

        #region Front Event
        private void lbl_players_Click(object sender, EventArgs e)
        {
            if (_isTimeToVote)
            {
                if (_playerVoted == null)
                {
                    ((Label)sender).BackColor = ButtonColor;
                    btn_sendVote.Enabled = true;
                    _playerVoted = _players.FirstOrDefault(x => x.Username == ((Label)sender).Text);
                }
                else if (_playerVoted?.Username == ((Label)sender).Text)//si tu déselectionne un joueur
                {
                    ((Label)sender).BackColor = Color.LightGray;
                    _playerVoted = null;
                    btn_sendVote.Enabled = false;
                }
                else return;
            }
            return;
        }
        private void btn_sendWord_Click(object sender, EventArgs e)
        {
            if (tb_wordPlayer.Text == string.Empty)
            {
                lbl_wordError.Visible = true;
                lbl_wordError.Text = "Please write a word";
                return;
            }
            if (!_isTimeToVote)
            {
                if (_wordsPlayers.Contains(tb_wordPlayer.Text))
                {
                    lbl_wordError.Visible = true;
                    lbl_wordError.Text = "Word already written";
                    return;
                }
                lbl_wordError.Visible = false;
                btn_sendWord.Enabled = false;
                _isPlayerTurn = false;
                SendWordToServer(tb_wordPlayer.Text);
            }
            else//Mr White word
            {
                lbl_wordError.Visible = false;
                btn_sendWord.Visible = false;
                _isPlayerTurn = false;
                SendWordToServer(tb_wordPlayer.Text);
            }
        }
        private void btn_sendVote_Click(object sender, EventArgs e)
        {
            SendVotePlayerToServer(_playerVoted.Id);
            btn_sendVote.Visible = false;
        }
        #endregion

    }
}
