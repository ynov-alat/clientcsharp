﻿using System;
using System.Windows.Forms;

namespace UndercoverGame
{
    partial class Play
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Play));
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.lbl_joinError = new System.Windows.Forms.Label();
            this.pb_loading = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_findLobby = new System.Windows.Forms.Label();
            this.lbl_joinLobby = new System.Windows.Forms.Label();
            this.cb_isCreatedLobbyPrivate = new System.Windows.Forms.CheckBox();
            this.btn_createLobby = new System.Windows.Forms.Button();
            this.btn_joinLobby = new System.Windows.Forms.Button();
            this.btn_findLobby = new System.Windows.Forms.Button();
            this.tb_joinLobby = new System.Windows.Forms.TextBox();
            this.lbl_createLobby = new System.Windows.Forms.Label();
            this.btn_backToMenu = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pb_loading)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // contextMenuStrip2
            // 
            this.contextMenuStrip2.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip2.Name = "contextMenuStrip2";
            this.contextMenuStrip2.Size = new System.Drawing.Size(61, 4);
            // 
            // lbl_joinError
            // 
            this.lbl_joinError.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_joinError.AutoSize = true;
            this.lbl_joinError.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_joinError.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_joinError.ForeColor = System.Drawing.Color.Orange;
            this.lbl_joinError.Location = new System.Drawing.Point(78, 452);
            this.lbl_joinError.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_joinError.Name = "lbl_joinError";
            this.lbl_joinError.Size = new System.Drawing.Size(260, 25);
            this.lbl_joinError.TabIndex = 12;
            this.lbl_joinError.Text = "Errors are displayed here ";
            this.lbl_joinError.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_joinError.Visible = false;
            // 
            // pb_loading
            // 
            this.pb_loading.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pb_loading.Image = global::UndercoverGame.Properties.Resources.loader;
            this.pb_loading.Location = new System.Drawing.Point(354, 506);
            this.pb_loading.Margin = new System.Windows.Forms.Padding(2);
            this.pb_loading.Name = "pb_loading";
            this.pb_loading.Size = new System.Drawing.Size(177, 164);
            this.pb_loading.TabIndex = 10;
            this.pb_loading.TabStop = false;
            this.pb_loading.Visible = false;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.Controls.Add(this.lbl_findLobby, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.lbl_joinLobby, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.cb_isCreatedLobbyPrivate, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.tb_joinLobby, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.lbl_createLobby, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(34, 122);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(835, 185);
            this.tableLayoutPanel1.TabIndex = 9;
            // 
            // lbl_findLobby
            // 
            this.lbl_findLobby.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_findLobby.AutoSize = true;
            this.lbl_findLobby.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_findLobby.Font = new System.Drawing.Font("Calisto MT", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_findLobby.ForeColor = System.Drawing.Color.White;
            this.lbl_findLobby.Location = new System.Drawing.Point(558, 0);
            this.lbl_findLobby.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_findLobby.Name = "lbl_findLobby";
            this.lbl_findLobby.Size = new System.Drawing.Size(275, 92);
            this.lbl_findLobby.TabIndex = 9;
            this.lbl_findLobby.Text = "Find a lobby";
            this.lbl_findLobby.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_joinLobby
            // 
            this.lbl_joinLobby.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_joinLobby.AutoSize = true;
            this.lbl_joinLobby.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_joinLobby.Font = new System.Drawing.Font("Calisto MT", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_joinLobby.ForeColor = System.Drawing.Color.White;
            this.lbl_joinLobby.Location = new System.Drawing.Point(280, 0);
            this.lbl_joinLobby.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_joinLobby.Name = "lbl_joinLobby";
            this.lbl_joinLobby.Size = new System.Drawing.Size(274, 92);
            this.lbl_joinLobby.TabIndex = 1;
            this.lbl_joinLobby.Text = "Join a lobby";
            this.lbl_joinLobby.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cb_isCreatedLobbyPrivate
            // 
            this.cb_isCreatedLobbyPrivate.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cb_isCreatedLobbyPrivate.AutoSize = true;
            this.cb_isCreatedLobbyPrivate.BackColor = System.Drawing.Color.Transparent;
            this.cb_isCreatedLobbyPrivate.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cb_isCreatedLobbyPrivate.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb_isCreatedLobbyPrivate.ForeColor = System.Drawing.Color.White;
            this.cb_isCreatedLobbyPrivate.Location = new System.Drawing.Point(94, 124);
            this.cb_isCreatedLobbyPrivate.Margin = new System.Windows.Forms.Padding(2);
            this.cb_isCreatedLobbyPrivate.Name = "cb_isCreatedLobbyPrivate";
            this.cb_isCreatedLobbyPrivate.Size = new System.Drawing.Size(89, 29);
            this.cb_isCreatedLobbyPrivate.TabIndex = 5;
            this.cb_isCreatedLobbyPrivate.Text = "private";
            this.cb_isCreatedLobbyPrivate.UseVisualStyleBackColor = false;
            // 
            // btn_createLobby
            // 
            this.btn_createLobby.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btn_createLobby.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(53)))), ((int)(((byte)(58)))));
            this.btn_createLobby.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_createLobby.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_createLobby.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_createLobby.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_createLobby.Location = new System.Drawing.Point(83, 342);
            this.btn_createLobby.Name = "btn_createLobby";
            this.btn_createLobby.Size = new System.Drawing.Size(168, 74);
            this.btn_createLobby.TabIndex = 4;
            this.btn_createLobby.Text = "Create";
            this.btn_createLobby.UseVisualStyleBackColor = false;
            this.btn_createLobby.Click += new System.EventHandler(this.btn_createLobby_Click);
            // 
            // btn_joinLobby
            // 
            this.btn_joinLobby.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btn_joinLobby.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(53)))), ((int)(((byte)(58)))));
            this.btn_joinLobby.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_joinLobby.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_joinLobby.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_joinLobby.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_joinLobby.Location = new System.Drawing.Point(363, 342);
            this.btn_joinLobby.Name = "btn_joinLobby";
            this.btn_joinLobby.Size = new System.Drawing.Size(168, 74);
            this.btn_joinLobby.TabIndex = 8;
            this.btn_joinLobby.Text = "Join";
            this.btn_joinLobby.UseVisualStyleBackColor = false;
            this.btn_joinLobby.Click += new System.EventHandler(this.btn_joinLobby_Click);
            // 
            // btn_findLobby
            // 
            this.btn_findLobby.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btn_findLobby.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(53)))), ((int)(((byte)(58)))));
            this.btn_findLobby.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_findLobby.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_findLobby.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_findLobby.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_findLobby.Location = new System.Drawing.Point(645, 342);
            this.btn_findLobby.Name = "btn_findLobby";
            this.btn_findLobby.Size = new System.Drawing.Size(168, 74);
            this.btn_findLobby.TabIndex = 11;
            this.btn_findLobby.Text = "Find";
            this.btn_findLobby.UseVisualStyleBackColor = false;
            this.btn_findLobby.Click += new System.EventHandler(this.btn_findLobby_Click);
            // 
            // tb_joinLobby
            // 
            this.tb_joinLobby.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tb_joinLobby.Location = new System.Drawing.Point(316, 128);
            this.tb_joinLobby.Margin = new System.Windows.Forms.Padding(2);
            this.tb_joinLobby.Name = "tb_joinLobby";
            this.tb_joinLobby.Size = new System.Drawing.Size(201, 20);
            this.tb_joinLobby.TabIndex = 10;
            // 
            // lbl_createLobby
            // 
            this.lbl_createLobby.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_createLobby.AutoSize = true;
            this.lbl_createLobby.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_createLobby.Font = new System.Drawing.Font("Calisto MT", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_createLobby.ForeColor = System.Drawing.Color.White;
            this.lbl_createLobby.Location = new System.Drawing.Point(2, 0);
            this.lbl_createLobby.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_createLobby.Name = "lbl_createLobby";
            this.lbl_createLobby.Size = new System.Drawing.Size(274, 92);
            this.lbl_createLobby.TabIndex = 0;
            this.lbl_createLobby.Text = "Create a lobby";
            this.lbl_createLobby.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btn_backToMenu
            // 
            this.btn_backToMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(53)))), ((int)(((byte)(59)))));
            this.btn_backToMenu.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_backToMenu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_backToMenu.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_backToMenu.ForeColor = System.Drawing.Color.White;
            this.btn_backToMenu.Location = new System.Drawing.Point(1079, 632);
            this.btn_backToMenu.Name = "btn_backToMenu";
            this.btn_backToMenu.Size = new System.Drawing.Size(178, 47);
            this.btn_backToMenu.TabIndex = 3;
            this.btn_backToMenu.Text = "Back to Menu";
            this.btn_backToMenu.UseVisualStyleBackColor = false;
            this.btn_backToMenu.Click += new System.EventHandler(this.btn_backToMenu_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::UndercoverGame.Properties.Resources.impostor;
            this.pictureBox2.Location = new System.Drawing.Point(836, 66);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(684, 656);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 16;
            this.pictureBox2.TabStop = false;
            // 
            // Play
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.ClientSize = new System.Drawing.Size(1269, 691);
            this.Controls.Add(this.lbl_joinError);
            this.Controls.Add(this.btn_findLobby);
            this.Controls.Add(this.btn_joinLobby);
            this.Controls.Add(this.btn_createLobby);
            this.Controls.Add(this.pb_loading);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.btn_backToMenu);
            this.Controls.Add(this.pictureBox2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(1285, 730);
            this.MinimumSize = new System.Drawing.Size(1285, 730);
            this.Name = "Play";
            this.Text = "Play";
            ((System.ComponentModel.ISupportInitialize)(this.pb_loading)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btn_backToMenu;
        private System.Windows.Forms.Button btn_createLobby;
        private System.Windows.Forms.CheckBox cb_isCreatedLobbyPrivate;
        private ContextMenuStrip contextMenuStrip1;
        private ContextMenuStrip contextMenuStrip2;
        private Button btn_joinLobby;
        private TableLayoutPanel tableLayoutPanel1;
        private Label lbl_createLobby;
        private Label lbl_joinLobby;
        private Label lbl_findLobby;
        private TextBox tb_joinLobby;
        private Button btn_findLobby;
        private PictureBox pb_loading;
        private Label lbl_joinError;
        private PictureBox pictureBox2;
    }
}