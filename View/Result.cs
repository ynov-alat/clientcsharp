﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UndercoverGame.Model;
using UndercoverGame.Service;

namespace UndercoverGame.View
{
    public partial class Result : ViewTemplate
    {
        private ResultModel _result;
        private List<User> _players;
        public Result()
        {
            InitializeComponent();           
            _result = LobbySocketService._instance.Result;
            ItinializeLabelsAndImages();
            InitializeTableLayoutPanel();
            InitializeViewWithTheme();
        }
        #region Initialization
        public void ItinializeLabelsAndImages()
        {
            foreach (Role role in _result.Winners)
            {
                switch (role)
                {
                    case Role.IMPOSTOR:
                        lbl_impostorWin.Visible = pb_impostor.Visible = true;
                        break;
                    case Role.CIVIL:
                        lbl_civiliansWin.Visible = pb_civilian.Visible = true;
                        break;
                    case Role.MR_WHITE:
                        lbl_mrWhiteWin.Visible = pb_MrWhite.Visible = true;
                        break;
                    default:
                        break;
                }
            }

            lbl_civilWord.Text = _result.CivilWord;
            lbl_guessMrWhite.Text = _result.GuessMrWhite;
            lbl_impostorWord.Text = _result.ImpostorWord;
        }
        public void InitializeTableLayoutPanel()
        {          
            _players = LobbySocketService._instance.Players;
           
            tlp_playersRoles.RowCount = _players.Count;
            tlp_playersRoles.RowStyles.RemoveAt(0);
            foreach (User user in _players)
            {
                int y = _players.FindIndex(x => x.Id == user.Id);
                tlp_playersRoles.RowStyles.Add(new RowStyle(SizeType.Percent, 100 / _players.Count));
                tlp_playersRoles.Controls.Add(GetNewLabel("lbl_" + _players.Find(x => x.Id == user.Id).Username, _players.Find(x => x.Id == user.Id).Username + " était : " + _result.UserRoles[y]), 0, y);
            }
        }
        public void InitializeViewWithTheme()
        {
            //Background
            this.BackColor = BackgndColor;

            //Text 
            label1.ForeColor = label2.ForeColor= label3.ForeColor =  lbl_civiliansWin.ForeColor = lbl_civilWord.ForeColor = lbl_guessMrWhite.ForeColor = lbl_impostorWin.ForeColor = lbl_impostorWord.ForeColor = lbl_mrWhiteWin.ForeColor = TextColor;

            //Button
            btn_leaveGame.BackColor= ButtonColor;

            //Text Button
            btn_leaveGame.ForeColor= TextButtonColor;
        }
        #endregion
        public Label GetNewLabel(string name, string text)
        {
            Label lbl = new Label();
            lbl.Anchor = ((((AnchorStyles.Top | AnchorStyles.Bottom) | AnchorStyles.Left) | AnchorStyles.Right));
            lbl.AutoSize = true;
            lbl.FlatStyle = FlatStyle.Flat;
            lbl.Location = new Point(285, 0);
            lbl.Size = new Size(277, 58);
            lbl.TextAlign = ContentAlignment.MiddleLeft;
            lbl.Font = new Font("Microsoft Sans Serif", 15F, FontStyle.Bold, GraphicsUnit.Point, ((byte)(0)));
            lbl.ForeColor = TextColor;
            lbl.Name = "lbl_" +name;
            lbl.Text = text;
            return lbl;
        }

        private void btn_leaveGame_Click(object sender, EventArgs e)
        {
            Form playForm = new Lobby();
            playForm.StartPosition = FormStartPosition.Manual;
            playForm.Left = this.Left;
            playForm.Top = this.Top;
            this.Visible = false;
            playForm.Show();
        }
    }
}
