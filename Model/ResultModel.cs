﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UndercoverGame.Model
{
    class ResultModel
    {  
        public Role[] Winners { get; set; }
        public Role[] UserRoles { get; set; }
        public string CivilWord { get; set; }
        public string ImpostorWord { get; set; }
        public string GuessMrWhite {get;set;}

        public ResultModel(Role[] winners,Role[] userRoles, string civilWord, string impostorWord, string guessMrWhite)
        {
            Winners = winners;
            UserRoles = userRoles;
            CivilWord = civilWord;
            ImpostorWord = impostorWord;
            GuessMrWhite = guessMrWhite;
        }
    }
}
