using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Sockets;
using UndercoverGame;
using UndercoverGame.Service;
using System.Configuration;

namespace UndercoverGame
{
    public class CurrentUser : User
    {
        public CurrentUser(string id, string username) : base(id, username)
        {
        }

    }
}