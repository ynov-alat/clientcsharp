using System;

namespace UndercoverGame
{
    public class User
    {
        public string Id { get; }
        public string Username { get; set; }
        public bool? BloodyTheme { get; set; }
        public bool? MustardTheme { get; set; }
        public bool? WineTheme { get; set; }
        public bool? SkyTheme { get; set; }
        public User(string id, string username)
        {
            Id = id;
            Username = username;
        }
    }
}