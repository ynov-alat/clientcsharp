﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UndercoverGame.Service;

namespace UndercoverGame.Model
{
    public static class TOPIC
    {
        public class CLIENT
        {
            public static readonly string CONNECTION = "CONNECTION";
            public static readonly string CONNECT_USER = "CLIENT/CONNECT_USER";
            public class LOBBY
            {
                public static readonly string CREATE = "CLIENT/LOBBY/CREATE";
                public static readonly string JOIN = "CLIENT/LOBBY/JOIN";
                public static readonly string LEAVE = "CLIENT/LOBBY/LEAVE";
                public static readonly string FIND = "CLIENT/LOBBY/FIND";
                public static readonly string START_GAME = "CLIENT/LOBBY/START_GAME";
            }

            public class GAME
            {
                public static readonly string PLAYER_WORD = "CLIENT/GAME/PLAYER_WORD";
                public static readonly string PLAYER_VOTE = "CLIENT/GAME/PLAYER_VOTE";
                public static readonly string MRWHITE_GUESS = "CLIENT/GAME/MR_WHITE_GUESS";
            }
        }

        public class SERVER
        {
            public class LOBBY
            {
                public static readonly string STATE = "SERVER/LOBBY/STATE";
                public static readonly string JOINED = "SERVER/LOBBY/JOINED";
                public static readonly string JOIN_ERROR = "SERVER/LOBBY/JOIN_ERROR";
                public static readonly string START_GAME = "SERVER/LOBBY/START_GAME";
            }

            public class GAME
            {
                public static readonly string PLAYERS_ORDER = "SERVER/GAME/PLAYERS_ORDER";
                public static readonly string PLAYER_THEME = "SERVER/GAME/PLAYER_THEME";
                public static readonly string PLAYER_TURN = "SERVER/GAME/PLAYER_TURN";
                public static readonly string VOTE_TURN = "SERVER/GAME/VOTE_TURN";
                public static readonly string PLAYER_VOTE = "SERVER/GAME/PLAYER_VOTE";
                public static readonly string RESULTS = "SERVER/GAME/RESULTS";

            }
        }


    }
}
