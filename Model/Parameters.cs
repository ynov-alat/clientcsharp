﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UndercoverGame.Model
{

    enum Role
    {
        IMPOSTOR,
        CIVIL,
        MR_WHITE,
    }

    public class Parameters
    {
        public int TurnMax { get; set; }
        public int ImpostorNumber { get; set; }
        public int TimeToWrite { get; set; }
        public int TimeToVote { get; set; }

        public Parameters(int maxTurn = 4, int impostorNumber = 1, int timeToWrite = 20, int timeToVote = 120)
        {
            this.TurnMax = maxTurn;
            this.ImpostorNumber = impostorNumber;
            this.TimeToWrite = timeToWrite;
            this.TimeToVote = timeToVote;
        }
    }
}
